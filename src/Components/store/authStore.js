import { configureStore, createSlice } from "@reduxjs/toolkit";


const authInitialState = {
    token:null,
    isUserLoggedIn: false
}

const authSlice = createSlice({
    name:"authentication",
    initialState:authInitialState,
    reducers:{
        login(state,action){
            state.token = action.payload,
            state.isUserLoggedIn = true
        }
        ,
        logout(state){
            state.token = null,
            state.isUserLoggedIn = false
        }
    }
})

export const authActions = authSlice.actions;

const store = configureStore({
    reducer: authSlice.reducer
})

export default store;