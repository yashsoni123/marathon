import React from "react";
import Heading from "../../UI/Heading/Heading";
import img1 from "../../../Images/GeneralInfo/start.png";
import Container from "../../UI/Container/Container";
import classes from "./GeneralInfo.module.css";
import img2 from "../../../Images/GeneralInfo/img2.png";
import img3 from "../../../Images/GeneralInfo/img3.png";
import img4 from "../../../Images/GeneralInfo/img4.png";
import img5 from "../../../Images/GeneralInfo/img5.jpg";
import img6 from "../../../Images/GeneralInfo/img6.jpg";
import img7 from "../../../Images/GeneralInfo/img7.png";
import img8 from "../../../Images/GeneralInfo/img8.png";
import img9 from "../../../Images/GeneralInfo/img9.png";
import img10 from "../../../Images/GeneralInfo/img10.png";
import img11 from "../../../Images/GeneralInfo/img11.png";
import img12 from "../../../Images/GeneralInfo/img12.png";
import img13 from "../../../Images/GeneralInfo/img13.png";
import img14 from "../../../Images/GeneralInfo/img14.png";
import img15 from "../../../Images/GeneralInfo/img15.png";
import img16 from "../../../Images/GeneralInfo/img16.png";
import img17 from "../../../Images/GeneralInfo/img17.png";
import img18 from "../../../Images/GeneralInfo/img18.png";
import img19 from "../../../Images/GeneralInfo/img19.jpg";
import SmallText from "../../UI/SmallText/SmallText";
import BoldText from "../../UI/BoldText/BoldText";
import Navbar from "../../UI/Navbar/Navbar";
import Footer from "../../UI/Footer/Footer";
import axios from "axios";

function GeneralInfo() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const on10KMPdfHandler = () => {
    axios({
      url: "http://localhost:9008/10KmPdf",
      method: "GET",
      responseType: "blob", // important
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "10KmRoutefile.pdf");
      document.body.appendChild(link);
      link.click();
    });
  };
  const on5KMPdfHandler = () => {
    axios({
      url: "http://localhost:9008/5KmPdf",
      method: "GET",
      responseType: "blob", // important
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "5KmRoutefile.pdf");
      document.body.appendChild(link);
      link.click();
    });
  };
  const on21KMPdfHandler = () => {
    axios({
      url: "http://localhost:9008/21KmPdf",
      method: "GET",
      responseType: "blob", // important
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "21KmRoutefile.pdf");
      document.body.appendChild(link);
      link.click();
    });
  };

  return (
    <>
      {/* <Navbar /> */}
      <div className={classes.generalInfo}>
        <Container>
          <div className="mx-auto">
            <Heading>Bhiwadi Half Marathon</Heading>
          </div>
          <div className="col-lg-12">
            <div className="row flex">
              <div className="col-md-4 col-sm-12 py-2">
                <img src={img1} height="160px" width="100%" alt="Start Image" />
              </div>
              <div className="col-md-8">
                <SmallText>
                  Organizing a Half Marathon can be a very enjoyable and
                  rewarding experience. Although there is more work involved
                  than most of runners might realize, making our first race
                  evolve from an idea into reality is ultimately very gratifying
                  for us. Below are some of the most important steps to consider
                  when planning, promoting, and executing our first 21Km half
                  marathon.
                </SmallText>
              </div>
            </div>
          </div>
          <h3>
            <span class="badge rounded-pill bg-dark" onClick={on5KMPdfHandler}>
              5KM
            </span>
            <span class="badge rounded-pill bg-dark" onClick={on10KMPdfHandler}>
              10KM
            </span>
            <span class="badge rounded-pill bg-dark" onClick={on21KMPdfHandler}>
              21KM
            </span>
          </h3>
          <div className="row mt-3 flex">
            <SmallText>
              Gone are the days when the half marathon and the marathon were
              events limited to long distance endurance runners competing for
              the race money, improve race performance. <br /> <br /> The
              archetype of the half marathon participant has dramatically
              changed. <b> The 21 Km Half marathon </b> distance is very
              appealing to the amateur running audience. The distance is long
              enough to be challenging to train for without requiring the
              commitment of a full marathon.
            </SmallText>
          </div>
          <div className="row flex">
            <p className={classes.itallicText}>
              The internet, social media, health consciousness present new
              challenges and opportunities for the people worldwide.{" "}
            </p>
          </div>
          <div className="row flex">
            <p className={classes.smallText}>
              My goal with this proposal is to share our runner perspective and
              insights to consider for half marathon event planning. We have
              organizing running events since 2019 in Bhiwadi, Rajasthan and
              other states.{" "}
            </p>
          </div>
          <div className="row">
            <p className={classes.boldText}>What motivates runners to run? </p>
          </div>
          <div className="row mt-3 center">
            <div className="col-md-4 px-3">
              <img src={img2} className="img-fluid" alt="" />
            </div>
            <div className="col-md-8">
              <p className={classes.smallText}>
                Walking is the simplest form of running. It is relatively easy
                and economical to put our shoes and get out of the door and
                walk. Now a days People trained themselves for half marathon or
                full marathon in with an organized running club program and meet
                some incredible people from all walk of life. Running continues
                to be part of our life and has provided unique adventures beyond
                the race event. As a part of local running community and the
                social running community on Facebook.{" "}
              </p>
            </div>
          </div>
          <div className="row mt-3">
            <p className={classes.smallText}>
              <b>“Why do we run” ?</b> Most of us share the enjoyment of running
              for the health benefits and travelling enjoyment but our
              perceptions and motivations differ. Some of our motivations
              include:
              <div className="col-md-10 mx-auto mt-3">
                <li>
                  <strong>Physical Health Benefits:</strong> Weight loss and
                  weight management, good exercise that builds strength and
                  stamina, a means to recover from other conditions and provides
                  improvement in sleep and digestion.
                </li>
                <li>
                  <strong>Psychological Benefits:</strong> Opportunities to
                  interact socially, to think and gain focus, get a natural
                  runner’s high, makes you feel strong and provides a sense of
                  personal accomplishment.
                </li>
                <li>
                  <strong>Stress Management:</strong> Stress reduction, a
                  relaxation activity for a caregiver.
                </li>
                <li>
                  <strong>
                    Keeping our body younger in preparation for the golden
                    years.{" "}
                  </strong>
                </li>
                <li>
                  <strong>Inspiration:</strong> From family, friends, role
                  models like MILIND SOMAN.
                </li>
                <li>
                  <strong>Flexibility:</strong> can run at our convenience. You
                  can walk or run at our own pace.
                </li>
                <li>
                  <strong>Travelling:</strong> Joy and exploration of places at
                  the ground level.
                </li>
              </div>
            </p>
          </div>
          <div className="row mt-3">
            <p className={classes.boldText}>
              How runners choose a half-marathon event?
            </p>
          </div>
          <div className="row mt-2">
            <p className={classes.smallText}>
              Running is a fun social activity that provides fun experience and
              opportunity to travel. Choosing a race is not as simple as one may
              imagine specially if it is not a local race. Many of us choose
              races based on destination: a new state, places we never been in,
              beautiful scenery, close to a well known or other sight of our
              interest, a fun location for a vacation. Destinations where we can
              visit family and friends.
            </p>
          </div>
          <div className="row mt-3">
            <p className={classes.smallText}>
              Convenience is critical in the decision making specially when
              considering a race that requires travelling. The race must fit the
              schedule and the schedule of accompanying travelers. The
              destination must be easily accessible by plane, train or vehicle
              transportation. Other considerations include what events of
              professional or personal interest for the participant or
              accompanying travelers our city is offering at the time of the
              event.
            </p>
          </div>
          <div className="row mt-3">
            <p className={classes.boldText}>
              Unique features of the race event:
            </p>
            <p className={classes.smallText}>
              <div className="col-10 mx-auto mt-3">
                <li>
                  <strong>Unique medals, Cool Theme.</strong>
                </li>
                <li>
                  <strong>Race organization: </strong> well organized, with
                  great sights, clear course markers, hydration and medical
                  attention if needed.
                </li>
                <li>
                  <strong>Race Course: </strong> scenic, interesting, shows
                  landmarks of the area.
                </li>
                <li>
                  <strong>Local support:</strong> spectators with fun and
                  inspiring signs, community offering food, drinks and playing
                  inspiring music (Rajasthani Cultural) add to the race
                  experience.
                </li>
                <li>
                  <strong>Post-race party:</strong> food, music, juices and
                  shakes etc.
                </li>
                <li>
                  <strong>Flexibility:</strong> can run at our convenience. You
                  can walk or run at our own pace.
                </li>
                <li>
                  Good expo that offers runners and visitors relevant
                  merchandise and services. A great place for runners traveling
                  from out of state for race to purchase gear or other running
                  needs that they may have forgotten at home.
                </li>
              </div>
            </p>
          </div>
          <div className="row mt-3">
            <p className={classes.boldText}>Online Race Registration Form:</p>
          </div>
          <div className="row flex mt-1">
            <div className="col-lg-10">
              {/* <p className={classes.smallText}> */}
              <SmallText>
                We can easily increase the participation of runners through the
                online registration process. As a online registration provider
                we make sure to utilize a simp le checkout process that doesn't
                require difficulty for runners. Single page in registration
                process makes it easier to complete registration forms, and we
                will make more money. Online fees can vary according to
                different race events.{" "}
              </SmallText>
              {/* </p> */}
            </div>
            <div className="col-lg-2">
              <img src={img3} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-4">
            <p className={classes.boldText}>Running Location:</p>
          </div>
          <div className="row flex my-3">
            <div className="col-lg-2 col-sm-6">
              <img src={img4} className="img-fluid" alt="Running Location" />
            </div>
            <div className="col-lg-10 ">
              {/* <p className={classes.smallText}> */}
              <SmallText>
                We organize our race in a location where a sizable portion of
                our prospective runners live. Depending on the expected size of
                race, we also considered the hotels are in the area to
                accommodate the runners.
              </SmallText>
            </div>
          </div>
          <div className="row mt-5">
            <BoldText>Race Course:</BoldText>
          </div>
          <div className="row flex my-1">
            <div className=" col-lg-6">
              <SmallText>
                We have a race course that gives runners a unique experience.
                This could mean a nice gentle downhill course, or perhaps
                conversely, a challenging uphill one, depending upon the
                demographic want to cater to. Often runners want a route that is
                flat or gentle downhill, but somewhere we make it challenging to
                make participants to surprise their standard mindset for the
                ideal running pace for the half marathon. We try to have an
                out-and-back or a loop course to avoid the traffic.
              </SmallText>
            </div>
            <div className=" mt-3 col-lg-6">
              <img src={img5} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-5">
            <BoldText>The Race Expo :</BoldText>
          </div>
          <div className="row mt-2 flex">
            <div className="col-lg-4">
              <img src={img6} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-8">
              <SmallText>
                Make the race expo a relevant event promoting a healthy life
                style for local and out of town community. Local expo visitors
                may be inspired and participate in race in future years. Keep in
                mind the out of town race participants. In addition to providing
                runner related product and services they are seeking additional
                products and services that tourists may need while away from
                home.
              </SmallText>
            </div>
          </div>
          <div className="row mt-3">
            <BoldText>What does race event offer?</BoldText>
            <br />
            <SmallText>
              We are primarily likely concerned about the race logistics
              including the start and finish line, the race course, the water,
              fueling, medical facilities. Our event includes other product
              offerings like T shirt, the race bib, the finisher medal and the
              pre and post race events. Consider each of these items as
              communication materials that carry the message that promotes our
              race.{" "}
            </SmallText>
          </div>

          <div className="row mt-3">
            <BoldText>Race Bib: </BoldText>
          </div>
          <div className="row">
            <div className="col-lg-8">
              <SmallText>
                In addition to provide runner identification and proof of
                registration the bib will be used to communicate multiple
                individual messages relevant race participants: team name,
                country of origin, supported charity. Bib are now available in
                all different sizes, designs and colors. It is the runner ticket
                to the race event related perks. Consider the opportunities to
                use the bib for social media, and as a tool for event organizers
                to target information to individual the runner needs.
              </SmallText>
            </div>
            <div className="col-lg-4">
              <img src={img7} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-3">
            <BoldText>Running Event T-Shirt: </BoldText>
          </div>
          <div className="row flex my-3">
            <div className="col-lg-3 col-sm-6 my-3">
              <img src={img8} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-9">
              <SmallText>
                Runners enjoy a great, fun, thoughtfully designed technical
                running t-shirt. However, not all runners may not wear the event
                t-shirt for the race. Race participants may be participating in
                the event as part of a corporate team or supporting a charity
                and choose to wear the race T shirt on a different occasion.
                From a runner perspective a poorly designed, bad quality T-shirt
                is a turn off.
              </SmallText>
            </div>
          </div>

          <div className="row mt-5">
            <BoldText>The Finisher Medal: </BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-10">
              <SmallText>
                Offering memorable bling. Race medals provide proof of
                completion of running goal. Take advantage of local artistic
                talent to design unique medals that will make race memorable.
                Medals travel back with race participants and are proudly
                displayed in their homes. Proud runners’ pictures with cool
                finisher medals provide great social media promotion for the
                event.{" "}
              </SmallText>
            </div>
            <div className="col-lg-2">
              <img src={img9} className="img-fluid" alt="" />
            </div>
          </div>

          <div className="row my-4">
            <BoldText>Aid Stations and Hydration:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-3 my-2">
              <img src={img10} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-9">
              <SmallText>
                Provide plenty of water, and if possible, another beverage with
                electrolytes to runners. Often aid stations are placed at a good
                distance. For longer runs the aid stations may include food, and
                pain relievers.
              </SmallText>
            </div>
          </div>
          <div className="row mt-5">
            <BoldText>Food:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-10">
              <SmallText>
                Depending on how long our race will be, we may consider having
                food in the finish area. Runners prefer food on the route that
                can be eaten easily and digested quickly. If possible, try to
                include food in our finish area that is high in quality, hygiene
                and protein. Often Participants can find a local market or
                restaurant to get food.{" "}
              </SmallText>
            </div>
            <div className="col-lg-2 col-sm-6">
              <img src={img11} className="img-fluid" alt="" />
            </div>
          </div>

          <div className="row mt-3">
            <BoldText>Awards:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-3 col-sm-6 my-4">
              <img src={img12} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-9">
              <SmallText>
                Our races have awards for age division winners. Sometimes these
                awards are just for{" "}
                <strong>
                  <i>top three places.</i>
                </strong>
                We choose to give out prize money. Event regulations stipulate
                that overall awards need to be based upon gun time. Age division
                awards can be based upon chip time.
              </SmallText>
            </div>
          </div>

          <div className="row mt-5">
            <BoldText>Portable Restrooms:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-9">
              <SmallText>
                Restrooms are a very important part of any race. The longer the
                duration of our event the more important they become. Even with
                a 5K race it is necessary to having portable restrooms at finish
                area at a minimum, and probably has some at our starting line
                and a couple on the route. These units can be very pricey but
                have to arrange for the hygiene concern for the female
                participant particularly.{" "}
              </SmallText>
            </div>
            <div className="col-lg-3 col-sm-6">
              <img src={img13} className="img-fluid p-3" alt="" />
            </div>
          </div>
          <div className="row mt-3">
            <BoldText>Event Safety:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-4 col-sm-8 my-3">
              <img src={img14} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-8">
              <SmallText>
                Coordinate the police and security for our event. Make sure that
                we will have the proper traffic safety devices like cones, and
                crowd control barricades if our venue requires them.
              </SmallText>
            </div>
          </div>
          <div className="row mt-5">
            <BoldText>
              Medical Considerations are Needed When Planning a Race:{" "}
            </BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-9">
              <SmallText>
                The distance of our event and the number of participants will
                determine the amount of medical personnel needed. We will have
                ambulance on the route, and a medical tent in the finish area
                with doctors and nurses handy to help with minor medical
                problems. It is wise to have an ambulance in the finish area to
                haul off runners that need serious medical attention. Smaller
                5K’s do not need much medical attention. It may be wise to
                consult with a sports medicine professional in our area to see
                what the specific needs of our event may be.{" "}
              </SmallText>
            </div>
            <div className="col-lg-3 col-sm-6">
              <img src={img15} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-3">
            <BoldText>Course Clean up:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-3 col-sm-8 my-3">
              <img src={img16} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-9">
              <SmallText>
                Clean up is often a job that is overlooked. This job is actually
                one of our most important jobs. Events can be banned from
                communities if they are not left as clean as they were found. It
                is generally a good idea to have fresh bodies of people in
                charge of cleanup that didn’t have to help with the set up. At
                the end of an event both we and our other volunteers will be
                exhausted, so try to have fresh people come in to help cleanup
              </SmallText>
            </div>
          </div>
          <div className="row mt-5">
            <BoldText>Other Supplies Needed to Organize a Race:</BoldText>
          </div>
          <div className="row flex mt-3">
            <div className="col-lg-8">
              <SmallText>
                We may want some or all of the following supplies: safety pins,
                bib numbers, zip ties, draw-string race bags, mile markers,
                directional signs, trash boxes, awards stand, results board,
                generators, sound system, finish structure, starting line
                structure, tents, tables, chairs, cups etc. There are many other
                supplies that could be considered depending on running event.
              </SmallText>
            </div>
            <div className="col-lg-4">
              <img src={img17} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-3">
            <BoldText>Event photography: </BoldText>
          </div>
          <div className="row flex mt-1">
            <div className="col-lg-3">
              <img src={img18} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-9">
              <SmallText>
                The memories of every event are very close to the event
                organizers as well as to the sponsors associated with the event.
                That is why we are trying to cover our event in best possible
                manner with multi camera positioning and DRONES to witness the
                sky to our event.
              </SmallText>
            </div>
          </div>
          <div className="row my-3">
            <BoldText>Runners Satisfaction:</BoldText>
          </div>
          <div className="row flex ">
            <div className="col-lg-8">
              <SmallText>
                One surprising aspect of running events is that few event
                organizers follow up with their runner participants to get
                feedback on the event. There is little opportunity to
                congratulate race organizer for their incredible job or to
                provide suggestions for future races.
              </SmallText>
            </div>
            <div className="col-lg-4 col-sm-8">
              <img src={img19} className="img-fluid" alt="" />
            </div>
          </div>
          <div className="row mt-3">
            <p className={classes.boldText}>Make our next event memorable:</p>
            <p className={classes.smallText}>
              <div className="col-10 mx-auto mt-3">
                <li>
                  Get to know our runners and their needs. Provide meaningful,
                  memorable event.
                </li>
                <li>
                  Use social media to enhance communication of the event and
                  runner experience keeping in mind that our event experience
                  starts at signing up and continues well beyond crossing the
                  finish line. Share our own journey on behind the scene race
                  planning and coordination. Consider engaging race volunteers,
                  the cheering squad and the community at large in our social
                  media efforts. Visuals, stories, fun facts can enhance the
                  interest in our event.
                </li>
                <li>
                  Involve our community. There is local talent that can
                  contribute to the success of our event. Engage local artist in
                  designing race swag, marketers, the local media, businesses to
                  promote our event.
                </li>
                <li>
                  Be environmentally friendly: engage our community in clean ups
                  before and after the race. Be an ambassador for our city.
                </li>
                <li>
                  Look beyond the running industry for marketing opportunities:
                  Corporate teams, charities, ancillary businesses.
                </li>
                <li>
                  Become the preferred event partner for runners, sponsors and
                  the community at large.
                </li>
              </div>
            </p>
          </div>
          <BoldText>The Future of Running Event Marketing</BoldText>
          <SmallText>
            Running event offerings will continue to expand and the likelihood
            of race participants running the same event more than once is very
            low. We consider the runner experience, implementing social media,
            partnering opportunities with other industries. Find our niche and
            offer a quality running experience for participants for many
            generations to come. See you at the start line
          </SmallText>
        </Container>
      </div>
    </>
  );
}

export default React.memo(GeneralInfo);
