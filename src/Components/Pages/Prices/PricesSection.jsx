import React from "react";
import { Link } from "react-router-dom";
import Navbar from "../../UI/Navbar/Navbar";
import "./PriceSection.css";

function PricesSection(props) {
  const k3m =
    "For Registration in 3Km Marathon So, Kindly contact to our Executive";
  const callSpan = <a href="tel:798877116">Get Register</a>;
  return (
    <>
      <div className="container-fluid my-5">
        <div className="my-2 box row  mx-auto  py-3  ">
          <div className="col-sm-12">
            <div className="row center">
              <div className="text-center col-md-3 col-sm-10">
                <span className="raceHeading">{props.name}</span>
                <h1 className="my-2 font-weight-bold h2 text-danger">
                  <sup>
                    <b>₹</b>
                  </sup>
                  <b>{props.price}</b>{" "}
                  <span className="delText">
                    <del>{props.exPrice}</del>
                  </span>{" "}
                </h1>
                {/* <small className="text-uppercase text-secondary">Offer Valid till 14<sup className="text-lowercase">th</sup> Jan 2022</small> */}
              </div>
              <div className="text-center col-md-6 mt-3 col-sm-12 px-2">
                <div className="gravText">
                  {props.id === "r1" ? k3m : props.text}
                </div>
              </div>
              <div className="text-center col-md-3 mt-4 col-10">
                {props.id === "r1" ? (
                  // <button className="btn text-white bg-dark px-5 py-3 main-btn">
                  //   <i className="fa fa-phone"></i> &nbsp; &nbsp; Call
                  // </button>
                  <a
                    className="btn text-white bg-dark px-5 py-3 main-btn"
                    href="tel:798877116"
                  >
                    <i className="fa fa-phone"></i> &nbsp; Register
                  </a>
                ) : (
                  <Link
                    to={`/registration/${props.name}`}
                    className="btn text-white bg-dark px-5 py-3 main-btn"
                  >
                    {props.id === "r1" ? callSpan : "Register"}
                  </Link>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default PricesSection;
