const raceCatogries = [
  {
    id: "r1",
    name: "FUN RUN (3KM)",
    price: 500,
    exPrice: 500,
    text: `Event T Shirt + Healthy Refreshment + Goodie Bag + Water Bottle + Towel + Die Casted "Medal" for the finishers + E-Certificate  + Photographs & Video + Personalized Bib + Medical support `,
  },
  {
    id: "r2",
    name: "BEGINNERS RUN (5KM)",
    price: 500,
    exPrice: 500,
    text: `Event T Shirt + Healthy Refreshment + Goodie Bag + Water Bottle + Towel + Die Casted "Medal" for the finishers + E-Certificate  + Photographs & Video + Personalized Bib + Medical support `,
  },
  {
    id: "r3",
    name: "PRO RUN (10KM)",
    price: 500,
    exPrice: 700,
    text: `Event T Shirt + Healthy Refreshment + Goodie Bag + Water Bottle + Towel + Die Casted "Medal" for the finishers + E-Certificate  + Photographs & Video + Personalized Bib + Medical support `,
  },
  {
    id: "r4",
    name: "ULTRA RUN (21KM) ",
    price: 500,
    exPrice: 900,
    text: `Event T Shirt + Healthy Refreshment + Goodie Bag + Water Bottle + Towel + Die Casted "Medal" for the finishers + E-Certificate  + Photographs & Video + Personalized Bib + Medical support `,
  },
];

export default raceCatogries;
