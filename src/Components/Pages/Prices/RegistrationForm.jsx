import axios from "axios";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import Heading from "../../UI/Heading/Heading";
import Navbar from "../../UI/Navbar/Navbar";
import classes from "./RegistrationForm.module.css";

function RegistrationForm() {
  const params = useParams();
  const raceName = params.id;
  const [response, setResponse] = useState("");
  setTimeout(() => {
    console.log("ts-iframe", document.getElementById("ts-iframe"));
  }, 1000);
  const [data, setData] = useState({
    firstname: "",
    lastname: "",
    email: "",
    phoneNumber: "",
    state: "",
    zip: "",
    address: "",
    gender: "",
    age: "",
    city: "",
  });

  const onSubmitFormHandler = (event) => {
    event.preventDefault();
    const {
      firstname,
      lastname,
      email,
      phoneNumber,
      state,
      zip,
      address,
      gender,
      age,
      city,
    } = data;

    if (
      firstname &&
      lastname &&
      email &&
      state &&
      zip &&
      phoneNumber &&
      gender &&
      address &&
      age &&
      city
    ) {
      axios
        .post(`http://localhost:9008/registration/${raceName}`, data)
        .then((res) => setResponse(res.data.message));
      console.log(data);
    } else {
      alert("WRONG");
    }
    setData({
      firstname: "",
      lastname: "",
      email: "",
      phoneNumber: "",
      state: "",
      zip: "",
      address: "",
      gender: "",
      age: "",
      city: "",
    });
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  return (
    <section className={classes.main}>
      {/* <Navbar /> */}
      <main className="col-md-12 col-sm-12 col-xs-12 mt-5 px-5 mx-auto">
        <Heading>Registration For {raceName}</Heading>
        <br />
        <div className="flex">
          {/* <iframe
            id="ts-iframe"
            src="https://www.townscript.com/v2/widget/bhiwadi-half-marathon-300130/booking"
            frameborder="0"
            height="600"
            width="80%"
          ></iframe>
          <link
            rel="stylesheet"
            href="https://www.townscript.com/static/Bookingflow/css/ts-iframe.style.css"
          ></link> */}
          <iframe
            id="ts-iframe"
            src="https://www.townscript.com/v2/widget/copy-of-bhiwadi-half-marathon-300130/booking"
            frameborder="0"
            height="600"
            width="80%"
          ></iframe>
          <link
            rel="stylesheet"
            href="https://www.townscript.com/static/Bookingflow/css/ts-iframe.style.css"
          ></link>
        </div>
      </main>
    </section>
  );
}

export default RegistrationForm;
