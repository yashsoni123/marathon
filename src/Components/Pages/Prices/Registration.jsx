import React from "react";
import PricesSection from "./PricesSection";
import classes from "./Registration.module.css";
import Container from "../../UI/Container/Container";
import raceCatogries from "./raceCategories";
import Heading from "../../UI/Heading/Heading";
import SmallText from "../../UI/SmallText/SmallText";
import Navbar from "../../UI/Navbar/Navbar";

function Registration() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const registrationCard = raceCatogries.map((item) => (
    <PricesSection
      price={item.exPrice}
      // exPrice={item.exPrice}
      text={item.text}
      id={item.id}
      name={item.name}
      key={item.id}
    />
  ));
  return (
    <>
      {/* <Navbar /> */}
      <header className={`${classes.header} flex`}>
        <Container>
          <div className={`${classes.mainText}`}>
            <span className={classes.redText}>NEVER </span>
            GIVING
            <br />{" "}
            <span className={classes.redText}>
              UPPP. <i className=" fa fa-hand-peace-o"></i>{" "}
            </span>
          </div>
        </Container>
      </header>
      <div className="mt-5">
        <div className="col-md-10 col-sm-11 col-xs-11 mt-5 mx-auto px-3">
          <Heading>REGISTRATION</Heading>
          <br />
          {/* <p className="px-4">
           
          </p> */}
          <SmallText>
            To apply for any of the race categories of the Bhiwadi Half Marathon
            2022, you need to submit an application form, along with the
            requisite application fees. Application forms received without fees
            will not be accepted. You can apply for your preferred race
            category, during the registration period, using following links:
          </SmallText>
        </div>

        <br />
        <div className="col-11 mx-auto">
          <Heading>FEE STRUCTURE</Heading>
        </div>
        <div className="col-sm-11 mx-auto">{registrationCard}</div>
        <br />
        <div className="col-10 mx-auto">
          <Heading>ELIGIBILITY CRITERIA</Heading>
        </div>
        <Container>
          <div className="mt-3">
            <SmallText>
              <strong>AGE ELIGIBILITY (2022) as for various races is :</strong>
            </SmallText>
            <div className="row">
              <table className="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">3 KM</th>
                    <th scope="col">5 KM</th>
                    <th scope="col">10 KM</th>
                    <th scope="col">21.1 KM</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="col">16 Years</th>
                    <th scope="col">16 Years</th>
                    <th scope="col">18 Years</th>
                    <th scope="col">18 Years</th>
                  </tr>
                </tbody>
              </table>
            </div>
            <SmallText>
              <li>
                Besides the age limit runners should be in good physical
                condition to adapt and be able to run at high altitude.
              </li>
              <li>
                The following criteria are mandatory for all runners for all
                races, except the <strong>05 km Bhiwadi Half Marathon</strong>.
                Entries for these categories will be screened and if you do not
                fulfill the requirement the organiser has the right to reject
                your application
              </li>
            </SmallText>
          </div>
        </Container>
        <div className="col-10 mx-auto">
          <Heading>TERMS & CONDITIONS</Heading>
        </div>
        <Container>
          <ol className="mt-3 px-1">
            <li>
              <SmallText>
                <b>Age limits :</b> These must be adhered to strictly and the
                organiser has the right to verify the age of participants
                before, during and after the race.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>Acceptance of Entries :</b>
              </SmallText>
              <ol type="a">
                <li>
                  <SmallText>
                    Please do not apply more than one entry per person. Multiple
                    applications will be rejected.
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Before sending in your application, please ensure that all
                    information as requested in the form has been provided with
                    else your application will not be accepted.
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Application fee – Please read this section before you
                    register.
                  </SmallText>
                </li>
              </ol>
            </li>
            <li>
              <SmallText>
                <b>Entries not accepted :</b> Participation in the event is
                subject to entry confirmation. Entries will not be accepted if:
              </SmallText>

              <ol>
                <li>
                  <SmallText>
                    Submission of incorrect documentation or information of your
                    entry.
                  </SmallText>
                </li>
              </ol>
            </li>
            <SmallText>
              In such cases the entry/registration shall be cancelled. The
              registration fee paid by you will be refunded after deducting a
              processing fee of Rs ₹500/-
            </SmallText>
            <li>
              <SmallText>
                <b>THE REGISTRATION FEE IS NON-REFUNDABLE : </b>for all races of
                Bhiwadi Half Marathon. No refund will be issued.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>Change of race category</b> is permitted up to 2022 by way of
                written intimation to the organisers. Participants will be
                charged Rs ₹500 towards processing fee. Please ensure timely
                written intimation for any change of race category.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>Transfer of registration to next year</b> is permitted up to
                2022 by way of written intimation to the organisers. Please
                ensure timely written intimation for transfer of registration..
              </SmallText>
            </li>
            <li>
              <SmallText>
                Entry or BIB is non-transferable to any other participant /
                person.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>BIB number and collection of bibs:</b>
              </SmallText>
              <ul>
                <li>
                  <SmallText>
                    Your official race BIB number is important. Therefore please
                    ensure that it is displayed during the entire race.
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Do not alter the Bib number in any manner
                  </SmallText>
                </li>
                <li>
                  <SmallText>Bib numbers are not transferable</SmallText>
                </li>
                <li>
                  <SmallText>No one else may wear your bib number</SmallText>
                </li>
                <li>
                  <SmallText>
                    The bib number must be pinned properly to the front of your
                    t-shirt and clearly visible
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Folding or covering any part of your bib number may result
                    in disqualification
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Do not forcefully bend, crease or fold your bib
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Make sure to fill out necessary information on the back side
                    of your Bib
                  </SmallText>
                </li>
              </ul>
            </li>
            <li>
              <SmallText>
                <b>Timing Chips:</b>
              </SmallText>
              <ul>
                <li>
                  <SmallText>
                    Bhiwadi Half Marathon will use a timing chip for the timed
                    races
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    Timing chips must be attached to your BIB to score the race
                    as per instructions provided
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    This timing chip must be worn on the race day, attached
                    properly to the BIB
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    The chip must be attached from Start to Finish in order to
                    be recognised as completing the race and receiving an
                    official finish time
                  </SmallText>
                </li>
                <li>
                  <SmallText>
                    The timing chip identifies your bib number and should not be
                    worn by any other participant
                  </SmallText>
                </li>
              </ul>
            </li>
            <li>
              <SmallText>
                <b> Collection of Bibs : </b>All confirmed participants are
                required to come to the Bhiwadi Half Marathon Hub to collect
                their running number bibs (and timing chips). No running number
                bib or timing chip will be posted to the individual mailing
                address. It is mandatory for the participants of all race
                categories to collect their running number bibs in person.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b> Race day images : </b> of all the races of the participants
                will be screened visually after the race. Where any of the
                participants is found not wearing the running bib allotted to
                him/her, the participant registered for that bib will be
                disqualified from the race and the subsequent edition of this
                event. The organiser reserves the rights to use race day images
                and videos of participants for the promotion of the event across
                all social media platforms.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b> Event cancellation : </b> Should the event be cancelled due
                to circumstances that are beyond the control of the organiser,
                all confirmed registrations for the year will automatically
                stand transferred to next year.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b> Instructions by the organisers and race officials : </b>{" "}
                must be followed with respect to all matters not provided
                herein. The organiser reserves the right to reject applications
                of participants who refuse to follow instructions of the race
                officials.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>
                  {" "}
                  Participants must ensure that they are medically and
                  physically fit to participate in the race. :{" "}
                </b>
                Any person who is suffering from any chronic disease such as
                heart disease or high blood pressure should not participate in
                the event. The organiser reserves the right to disallow /
                disqualify any participant who is known or suspected to be
                physically unfit to participate in the event.
              </SmallText>
            </li>
            <li>
              <SmallText>
                <b>
                  {" "}
                  The application form and the right to participate in the event
                  and the rights and benefits available to the participant under
                  this application form is at the sole discretion of the event
                  organisers and promoters, and cannot be transferred to any
                  other participant / person under any circumstances. The actual
                  participant alone shall be entitled to the rights and benefits
                  arising out of such confirmation of participation.{" "}
                </b>
              </SmallText>
            </li>
          </ol>
        </Container>
      </div>
    </>
  );
}

export default React.memo(Registration);
