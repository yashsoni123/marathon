import React from "react";
import Container from "../../UI/Container/Container";
import "./TeamSection.css";
import Heading from "../../UI/Heading/Heading";
import coachPic from "../../../../src/Images/coachSunilYogi.jpeg";
import devenBhaiya from "../../../../src/Images/devenBhaiya.jpeg";
import "./TeamSection.css";
function TeamSection() {
  return (
    <>
      <div className="col-10 mx-auto">
        <Heading>OUR TEAM</Heading>
        <br />
      </div>
      <section className="teamSection">
        <Container>
          <br />
          {/* <div className="card col-md-4  mx-auto text-center">
            <img src={coachPic} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title h2">Mr Virat Kohli</h5>
              <h5 className="card-title h4">Founder</h5>
            </div>
          </div> */}
          <div className="row">
            <div className="card col-md-3 col-sm-8 mt-3 mx-auto">
              <img
                src={coachPic}
                className="card-img-top"
                alt="Coach SUNIL YOGI"
              />
              <div className="card-body">
                <h6 className="card-title h3">Coach SUNIL YOGI</h6>
                <h6 className="card-title h6">
                  2014 - Junior national athletics
                </h6>
              </div>
            </div>
            <div className="card col-md-3 col-sm-8 mt-3 mx-auto">
              <img
                style={{ height: "225px" }}
                src={devenBhaiya}
                className="card-img-top"
                alt="श्री देवेन नाहरवाल (दाऊ भैया)"
              />
              <div className="card-body d-flex align-items-center justify-content-center flex-column">
                <h5 className="card-title h3">श्री देवेन नाहरवाल (दाऊ भैया)</h5>
                <h5 className="card-title h5">पटवारी, दिल्ली सरकार</h5>
              </div>
            </div>
          </div>
          <br />
          {/* 
        <div className="row text-center">
        <div className="card col-md-3 col-sm-8 mt-3 mx-auto ">
          <img src={teamPic1} className=" img-fluid" alt="..." />
          <div className="card-body secondary">
            <h5 className="card-title h2">Mr Virat Kohli</h5>
            <h5 className="card-title h4">Manager</h5>
          </div>
        </div>
        <div className="card col-md-3 col-sm-8 mt-3 mx-auto">
          <img src={teamPic1} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title h2">Mr Virat Kohli</h5>
            <h5 className="card-title h4">Manager</h5>
          </div>
        </div>
        <div className="card col-md-3 col-sm-8  mt-3 mx-auto">
          <img src={teamPic1} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title h2">Mr Virat Kohli</h5>
            <h5 className="card-title h4">Member</h5>
          </div>
        </div>
        </div> */}
          {/* <div className="container1">
            <div className="card1">
              <div className="image1">
                <img src={coachPic} />
              </div>
              <div className="content1">
                <h3>Coach SUNIL YOGI</h3>
                <p>
                  2014 - Junior national athletics championship BANGLORE 400mtr
                  Silver
                </p>
              </div>
            </div>
            <div className="card1">
              <div className="image1">
                <img href="#" src={devenBhaiya} />
              </div>
              <div className="content1">
                <h3>श्री देवेन नाहरवाल (दाऊ भैया)</h3>
                <p>पटवारी, दिल्ली सरकार</p>
              </div>
            </div>
          </div> */}

          <br />
        </Container>
        <br />
      </section>
    </>
  );
}

export default TeamSection;
