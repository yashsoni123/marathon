import React, { useState } from "react";
import AboutSection from "../Home/AboutSection";
import TeamSection from "./TeamSection.jsx";
import classes from "./AboutUs.module.css";
import Container from "../../UI/Container/Container";
import Navbar from "../../UI/Navbar/Navbar";
import SmallText from "../../UI/SmallText/SmallText";
import p1 from "../../../Images/Home/Pillars1.jpeg";
import p2 from "../../../Images/Home/Pillars2.jpeg";
import p3 from "../../../Images/Home/Pillars3.jpeg";
import p4 from "../../../Images/Home/Pillars41.jpeg";
import Modal from "../Home/Modal";
import Heading from "../../UI/Heading/Heading";

const AboutUs = () => {
  const [title, setTitle] = useState("");
  const [para, setPara] = useState("");

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const onModal3ClickHandler = () => {
    setTitle("Dr. Roop Singh");
    setPara(
      `Dr. Roop Singh is one of the best general physicians in Bhiwadi. He is experienced with 20 years as a consultant physician of Internal Medicine. . Dr.Roop Singh is considered the best physician in Bhiwadi.`
    );
  };

  const onModal1ClickHandler = () => {
    setTitle("Mr. Neeraj Jhalani");
    setPara(`Khandelwal Agencies is NCR’s Largest Industrial Electrical House  We are serving from Last 25 Year as a Leading Distributor of above  25 National & International Brands in Industrial & Commercial Sector.
      We are a one stop solution for Low Voltage SwitchGear/Motors / Drives / Medium Voltage SwitchGear, All type of Electrical Panels,Wire & Cables/Industrial Lighting/Reactor & Capacitor/Cable lugs & Glands.
      We are No.1 Channel Partner For ABB SwitchGear in Tier 2 Cities in INDIA as well as Rajasthan.We are working with more than 1500 Industrial Customers accross India.We have 30000+ Above RCC Structure for Warehouse with ready stock of 10000+ products.`);
  };

  const onModal2ClickHandler = () => {
    setTitle("Dr Rajesh Choudhary MD");
    setPara(`Dr Rajesh Choudhary MD (Radio-diagnosis) is Senior Radiologist and Managing Director at Bharat Ultrasound and Diagnostic Centre Bhiwadi. He has been trained in latest radiological techniques from the best institutes in India, PGI Chandigarh. He is life member of IRIA. He is  past president of Indian Medical Association Bhiwadi (2019-2022). He is member of Rotary Club Bhiwadi, BMA, BCCI, BVP and GNHDCA Gurugram. His wife Dr Shruti Sangwan is also MD (Radiodiagnosis) and eminent radiologist and President of Indian Medical Association Bhiwadi (2021-23). 
      Bharat Diagnostic Centre was started by Dr Shruti Sangwan and Dr Rajesh Choudhary in 2004. It was the first diagnostic centre of Bhiwadi. Today it is the biggest diagnostic centre of Bhiwadi with latest and most advanced machines including, Ultrasound, Colour Doppler, Echocardiography, CT Scan, MRI, Digital X-Ray (DR), TMT, PFT, Interventional Radiology and Fully automatic pathology laboratory with two full time radiologists and one full time pathologist. It has a chain of diagnostic centres.`);
  };
  const onModal4ClickHandler = () => {
    setTitle("Mr. IMRAN KHAN");
    setPara(
      `Managing Director ECR build tech pvt. Ltd. *प्रदेश महासचिव, बहुजन समाज पार्टी, राजस्थान. *पूर्व प्रत्याशी लोकसभा, अलवर, राजस्थान.`
    );
  };
  return (
    <>
      {/* <Navbar /> */}
      <header className={`${classes.header}`}>
        <Container>
          <div className={`${classes.mainText}`}>
            <span className={classes.redText}> INDIA' S </span> PREMIERE
            <br /> <span className={classes.redText}>ENDURANCE </span> SPORTS
            <br /> <span className={classes.redText}>PLATFORM. </span>
          </div>
        </Container>
      </header>
      <AboutSection />
      <div className="col-sm-11 mx-auto px-4">
        <Heading>Pillars of Strength</Heading>
      </div>
      <br />
      <div className="container-fluid my-5">
        <div className="col-11 mx-auto">
          <div className="row px-3">
            <div
              className={`${classes.pilarsBox} col-sm-2 col-md-3 text-center`}
            >
              <img src={p2} alt="" className="img-thumbnail my-1" />
              <SmallText>
                <b>Mr. Neeraj Jhalani</b> Khandelwal Agencies is NCR’s Largest
                Industrial Electrical House We are serving from Last 25 Year as
                a Leading Distributor of above 25 National
              </SmallText>
              <button
                type="button"
                className="btn btn-primary"
                data-bs-toggle="modal"
                data-bs-target="#staticBackdrop"
                onClick={onModal1ClickHandler}
              >
                Know More...
              </button>
            </div>
            <div
              className={`${classes.pilarsBox} col-sm-2 col-md-3 text-center`}
            >
              <img src={p1} alt="" className="img-thumbnail my-1 " />
              <SmallText>
                <b>Dr Rajesh Choudhary MD </b> (Radio-diagnosis) is Senior
                Radiologist and Managing Director at Bharat Ultrasound and
                Diagnostic Centre Bhiwadi.
              </SmallText>
              <button
                type="button"
                className="btn btn-primary my-3"
                data-bs-toggle="modal"
                data-bs-target="#staticBackdrop"
                onClick={onModal2ClickHandler}
              >
                Know More...
              </button>
            </div>
            <div
              className={`${classes.pilarsBox} col-sm-3 col-md-3 text-center`}
            >
              <img src={p3} alt="" className="img-thumbnail my-1" />
              <SmallText>
                <b>Dr. Roop Singh</b> is one of the best general physicians in
                Bhiwadi. He is experienced with 20 years as a consultant
                physician of Internal Medicine.
              </SmallText>
              <button
                type="button"
                className="btn btn-primary mt-3"
                data-bs-toggle="modal"
                data-bs-target="#staticBackdrop"
                onClick={onModal3ClickHandler}
              >
                Know More...
              </button>
            </div>
            <div
              className={`${classes.pilarsBox} col-sm-2 col-md-3 text-center`}
            >
              <img src={p4} alt="" className="img-thumbnail" />
              <SmallText>
                <b>Mr. IMRAN KHAN</b>, Managing Director ECR build tech pvt.
                Ltd. *प्रदेश महासचिव, बहुजन समाज पार्टी, राजस्थान. *पूर्व
                प्रत्याशी लोकसभा, अलवर, राजस्थान.
              </SmallText>
              <button
                data-bs-toggle="modal"
                data-bs-target="#staticBackdrop"
                onClick={onModal4ClickHandler}
                className="btn btn-primary"
              >
                Know More...
              </button>
            </div>
          </div>
        </div>
      </div>
      <TeamSection />

      <Modal title={title} para={para} />
    </>
  );
};

export default AboutUs;
