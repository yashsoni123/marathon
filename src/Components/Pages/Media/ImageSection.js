import React from 'react';
import classes from "./ImageSection.module.css";

const ImageSection = props => {
    return (
        <div className='col-sm-6'>
            <div className={classes.box}><img src={props.src} alt=""  className={`${classes.image}`}  /></div>
 
        </div>
    )
}

export default ImageSection
