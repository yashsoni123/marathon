import React, { Fragment } from "react";
import Container from "../../UI/Container/Container";
import Heading from "../../UI/Heading/Heading";
import Navbar from "../../UI/Navbar/Navbar";
import getData from "./getData";
import classes from "./Media.module.css";
import ImageSection from "./ImageSection";
import CarasoulSection from "./CarasoulSection";
import VediosSection from "./VediosSection";
import vedio1 from "../../../Images/Media/vedio1.mp4";
import vedio2 from "../../../Images/Media/vedio2.mp4";
import vedio3 from "../../../Images/Media/vedio3.mp4";

function Media() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  let imagesData = getData();
  const images = imagesData.map((item) => (
    <ImageSection key={item.id} src={item.src} />
  ));
  return (
    <>
      {/* <Navbar /> */}
      <section className={classes.mediaSection}>
        <Container>
          <Heading>Image Gallery</Heading>
          <div className="px-4">
            <CarasoulSection />
          </div>
          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingOne">
                <button
                  class="accordion-button h1"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                  Images Of Promo Run
                </button>
              </h2>
              <div
                id="collapseOne"
                class="accordion-collapse collapse show"
                aria-labelledby="headingOne"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <div className=" row mx-auto">{images}</div>
                </div>
              </div>
            </div>
          </div>
          <div className="my-5">
            <Heading>Video Gallery</Heading>
          </div>
          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTwo">
                <button
                  class="accordion-button h3 collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseTwo"
                  aria-expanded="true"
                  aria-controls="collapseTwo"
                >
                  Videos Of Promo Run
                </button>
              </h2>
              <div
                id="collapseTwo"
                class="accordion-collapse collapse"
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <div className=" row mx-auto">
                    <VediosSection vedio={vedio1} />
                    <VediosSection vedio={vedio2} />
                    <VediosSection vedio={vedio3} />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <br />
        </Container>
      </section>
    </>
  );
}

export default React.memo(Media);
