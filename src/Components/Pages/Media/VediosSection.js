import React from "react";
import Container from "../../UI/Container/Container";
import ReactDOM from "react-dom";
import ReactPlayer from "react-player";
import classes from "./VediosSection.module.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

const VediosSection = props => {
  return (
    <div className="mt-3 mx-auto">
      <Container>

        <div className={`${classes.vedioBox} col-sm-12`}>
          <video controls width="100%" height="300px">
            <source src={props.vedio} type="video/webm" />
            Sorry, your browser doesn't support embedded videos.
          </video>
          <br />
        </div>

      </Container>
    </div>
  );
};
export default VediosSection;
