import img1 from "../../../Images/Media/1.jpeg";
import img2 from "../../../Images/Media/img2.jpeg";
import img3 from "../../../Images/Media/img3.jpeg";
import img4 from "../../../Images/Media/img4.jpeg";
import img5 from "../../../Images/Media/img5.jpeg";
import img6 from "../../../Images/Media/img6.jpeg";
import img7 from "../../../Images/Media/img7.jpeg";
import img8 from "../../../Images/Media/img8.jpeg";
import img9 from "../../../Images/Media/img9.jpeg";
import img10 from "../../../Images/Media/img10.jpeg";
import img11 from "../../../Images/Media/img11.jpeg";
import img12 from "../../../Images/Media/img12.jpeg";
import img13 from "../../../Images/Media/img13.jpeg";
import img14 from "../../../Images/Media/img14.jpeg";
import img15 from "../../../Images/Media/img15.jpeg";
import img16 from "../../../Images/Media/img16.jpeg";
import img17 from "../../../Images/Media/img17.jpeg";


const getData = () => {
    let data = [
      {
        id:'img1',
        src: `${img1}`,
      },
      {
        id:'img2',
        src: `${img2}`,
      },
      {
        id:'img3',
        src: `${img3}`,
      },
      {
        id:'img4',
        src: `${img4}`,
      },
      {
        id:'img5',
        src: `${img5}`,
      },
      {
        id:'img6',
        src: `${img6}`,
      },
      {
        id:'img7',
        src: `${img7}`,
      },
      {
        id:'img8',
        src: `${img8}`,
      },
      {
        id:'img9',
        src: `${img9}`,
      },
      {
        id:'img10',
        src: `${img10}`,
      },
      {
        id:'img11',
        src: `${img11}`,
      },
      {
        id:'img12',
        src: `${img12}`,
      },
      {
        id:'img13',
        src: `${img13}`,
      },
      {
        id:'img14',
        src: `${img14}`,
      },
      {
        id:'img15',
        src: `${img15}`,
      },
  
      
    ]
    return data;
  }
  
  export default getData;