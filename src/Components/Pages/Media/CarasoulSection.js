import React, { Component } from "react";
import ReactDOM from "react-dom";
import classes from "./CarasoulSection.module.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import img1 from "../../../Images/Media/img5.jpeg";
import img12 from "../../../Images/Media/img12.jpeg";
import img3 from "../../../Images/Media/img7.jpeg";
import img15 from "../../../Images/Media/img15.jpeg";
import img10 from "../../../Images/Media/img10.jpeg";

const CarasoulSection = () => {
  return (

    <div className="mx-auto">
      <Carousel autoPlay={true} interval={2300} infiniteLoop={true}>
        <div className={classes.image}>
          <img src={img3} className="img-fluid"/>
          {/* <p className="legend">Bhiwadi Marathon Promo Run</p> */}
        </div>
        <div className={classes.image}>
          <img src={img1}  className="img-fluid" />
          {/* <p className="legend">Bhiwadi Marathon Promo Run</p> */}
        </div>
        <div className={classes.image}>
          <img src={img12}  className="img-fluid"/>
          {/* <p className="legend">Legend 2</p> */}
        </div>
        <div className={classes.image}>
          <img src={img15} className="img-fluid" />
          {/* <p className="legend">Legend 2</p> */}
        </div>
        <div className={classes.image}>
          <img src={img15}  className="img-fluid"/>
          {/* <p className="legend">Legend 2</p> */}
        </div>
      </Carousel>
    </div>
  );
};

export default CarasoulSection;
