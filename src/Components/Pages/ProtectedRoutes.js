import React, { useEffect, useState } from "react";
import { Redirect, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useCookies } from "react-cookie";
import { authActions } from "../store/authStore";
import axios from "axios";
import { useLocation } from "react-router-dom";

const ProtectedRoutes = (props) => {
  const { isAuth: isAuth, component: Component, ...rest } = props;
  const token = useSelector((state) => state.token);
  const isUserLoggedIn = useSelector((state) => state.isUserLoggedIn);
  const history = useHistory();
  const dispatch = useDispatch();
  const [error, setError] = useState("");
  const authState = useSelector((state) => state);
  const CM = props.CM;
  const [cookies] = useCookies(["bhiwadiHalfMarathon"]);

  const location = useLocation();
  // console.log("Hello World", cookies.bhiwadiHalfMarathon);

  // useEffect(() => {
  //   if (cookies.bhiwadiHalfMarathon === authState.token) {
  //     console.log("Succesfully Logged In");
  //     dispatch(authActions.login(cookies.bhiwadiHalfMarathon));
  //     history.push("/admin-home");
  //   }
  // }, []);

  useEffect(() => {
    if (!localStorage.getItem("authToken")) {
      history.push("/admin-login");
    }
    const fetchPrivateData = async () => {
      const config = {
        header: {
          "Content-Type": "application/json",
          authorization: `Bearer ${localStorage.getItem("authToken")}`,
        },
      };
      try {
        const { data } = await axios.get(
          `http://localhost:9008${location.pathname}`,
          config
        );
        // setPrivateData(data.data);
        console.log("data", data);
        console.log("data.dataa", data.data);
      } catch (err) {
        // localStorage.removeItem("authToken");
        console.log("You are not authorized please login");
        setError("You are not authorized please login");
      }
    };

    fetchPrivateData();
  }, []);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (localStorage.getItem("authToken")) {
          return <Component />;
        } else {
          return (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
          );
        }
      }}
    ></Route>
  );
};

export default ProtectedRoutes;
