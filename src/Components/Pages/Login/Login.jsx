import axios from "axios";
import React, { useState } from "react";
import Heading from "../../UI/Heading/Heading";
import Navbar from "../../UI/Navbar/Navbar";
import classes from "./Login.module.css";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import Container from "../../UI/Container/Container";
import login from "../../../Images/login1.png";
import { authActions } from "../../store/authStore";
import { useCookies } from "react-cookie";

const Login = (props) => {
  const [cookie, setCookies] = useCookies();
  const [responseMsg, setResponseMsg] = useState("");
  const [responseAuth, setResponseAuth] = useState(false);
  const [userLogged, setUserLogged] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const onSubmitHandler = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:9008/login", data)
      .then((res) => {
        setResponseAuth(res.data.message);
        setResponseMsg(res.data.message);
        console.log(res.data);
        if (res.data.auth) {
          setUserLogged(true);
          dispatch(authActions.login(res.data.token));
          localStorage.setItem("authToken", res.data.token);
          setTimeout(() => {
            history.push("/admin-home");
          }, 3000);
        }
      })
      .catch((err) => {
        alert(err);
      });
    data.email = "";
    data.password = "";
  };

  // try {
  //   const { data } = await axios.post(
  //     "/api/auth/login",
  //     { email, password },
  //     config
  //   );

  //   localStorage.setItem("authToken", data.token);
  // } catch (error) {
  //   setError(error.response.data.error);
  //   setTimeout(() => {
  //     setError("");
  //   }, 5000);
  // }
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const onInputChangeHandler = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };
  return (
    <>
      {/* <Navbar /> */}
      <section className={classes.login}>
        <Container>
          <Heading>Admin Login</Heading>
          <form className="  mx-auto" onSubmit={onSubmitHandler}>
            <div className={`${classes.loginBox} row center`}>
              <div className="col-sm-6">
                <img
                  src={login}
                  className={`${classes.animated} img-fluid`}
                  alt=""
                />
              </div>
              <div className={`${classes.loginForm} col-sm-6 mt-2`}>
                <div className="mb-3 col-md-10 mx-auto col-sm-12">
                  {responseMsg === "Admin Not Found" && (
                    <div className={`${classes.errorResponse} my-3`}>
                      <i className="fa fa-user-times  "></i> {responseMsg}
                    </div>
                  )}
                  {responseMsg === "Something went wrong!!!" && (
                    <div className={`${classes.alreadyResponse} my-3`}>
                      <i className="fa fa-minus-circle "></i> {responseMsg}
                    </div>
                  )}
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Email address
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    id="exampleInputEmail1"
                    name="email"
                    value={data.email}
                    aria-describedby="emailHelp"
                    onChange={onInputChangeHandler}
                  />
                </div>
                <div className="mb-3 col-md-10 mx-auto col-sm-12">
                  <label htmlFor="exampleInputPassword1" className="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    value={data.password}
                    className="form-control"
                    id="exampleInputPassword1"
                    onChange={onInputChangeHandler}
                  />
                </div>
                <button
                  type="submit"
                  className={`${classes.loginBtn} btn col-sm-10 btn-primary`}
                >
                  Login
                </button>
              </div>
            </div>
          </form>
        </Container>
      </section>
    </>
  );
};

export default React.memo(Login);
