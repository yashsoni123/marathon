import React from "react";
import Container from "../../UI/Container/Container";
import classes from "./ContactUs.module.css";
import contactUsImg from "../../../Images/ContactUs-1.png";
import Heading from "../../UI/Heading/Heading";
import Navbar from "../../UI/Navbar/Navbar";
import Footer from "../../UI/Footer/Footer";
import BoldText from "../../UI/BoldText/BoldText";
import SmallText from "../../UI/SmallText/SmallText";

function ContactUs() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      {/* <Navbar /> */}
      <div className="mt-2">
        <header className={`${classes.header} flex`}>
          <Container>
            <div className="row pt-3 flex">
              <div className="col-md-5 mt-3 ">
                <div className={classes.head1}>
                  <span className={classes.colorBlue}>Contact</span> Us
                </div>
                <div className={classes.head2}>
                  Have a Question or
                  <span className={classes.colorLightBlue}>
                    &nbsp; Need Assistance?
                  </span>
                </div>
                <div className={classes.head3}>
                  We are here to help you
                  <span className={classes.colorLightBlue}>
                    &nbsp;with anything you need !
                  </span>
                </div>
              </div>
              <div className="col-md-5">
                <img
                  src={contactUsImg}
                  alt=""
                  className={`${classes.animated} img-fluid`}
                />
              </div>
            </div>
            <div>
              <Heading>GET IN TOUCH</Heading>
              <div className="row center px-3">
                <div className="col-md-5 ">
                  <SmallText>
                    {" "}
                    <i className=" fa fa-envelope"></i>{" "}
                    <a
                      className={classes.link}
                      href="mailto:bhiwadihalfmarathon@gmail.com"
                    >
                      bhiwadihalfmarathon@gmail.com
                    </a>{" "}
                  </SmallText>
                  <SmallText>
                    <i className="fa fa-phone"></i>{" "}
                    <a className={classes.link} href="tel:798877116">
                      +91-798877116
                    </a>
                    &nbsp;
                    <a className={classes.link} href="tel:7976135519">
                      7976135519
                    </a>
                  </SmallText>
                  <SmallText></SmallText>
                </div>
                <div className="col-md-7">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56257.92741343105!2d76.80502457147296!3d28.20365244262949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d378ebd6b8223%3A0x907abc8f1e4f815c!2sBhiwadi%2C%20Rajasthan!5e0!3m2!1sen!2sin!4v1642622770691!5m2!1sen!2sin"
                    width="100%"
                    height="290px"
                    allowfullscreen=""
                    loading="lazy"
                  />
                </div>
              </div>
            </div>
          </Container>
        </header>
      </div>
      {/* <Footer /> */}
    </>
  );
}

export default React.memo(ContactUs);
