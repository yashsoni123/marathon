import React from 'react'

const TableRow = props => {
    return (
        <tr>
                      <td>{props.Registration_Id}</td>
                      <td><b>{props.Attendee_Name}</b></td>
                      <td><b>{props.Attendee_Email}</b></td>
                    <td>{props.Status}</td>
                      <td>{ props.Ticket_Name}</td>
                      <td>{ props.Order_Id}</td>
                      <td>{ props.Title}</td>
                      <td>{ props.DOB}</td>
                      <td>{ props.Gender}</td>
                      <td><b>{ props.Contact_Number}</b></td>
                      <td>{ props.RACE_EVENT}</td>
                      <td>{ props.Address}</td>
                      <td>{ props.City}</td>
                      <td>{ props.Pincode}</td>
                      <td>{ props.Emergency_Contact_Name}</td>
                      <td>{ props.Emergency_Contact_Number}</td>
                      <td>{ props.Emergency_Contact_Relation}</td>
                      <td>{ props.Blood_Group}</td>
                      <td>{ props.T_SHIRT_SIZE}</td>
                      <td>{ props.ADD_COUPON}</td>
                      <td>{ props.Attendee_Check_In}</td>
                      <td>{ props.Check_In_Time}</td>
                      <td>{ props.Attendee_Badge_Print}</td>
                      <td>{ props.Badge_Print_Time}</td>
                    </tr>
    )
};


export default TableRow;