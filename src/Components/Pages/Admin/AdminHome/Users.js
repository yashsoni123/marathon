import React from "react";
import { USER_PER_PAGE } from "../../../constraints";
import TableRow from "./TableRow";
import classes from "./Users.module.css";
import Pagination from "../../../Pagination";

function Users({ users, page }) {
  const startIndex = (page - 1) * USER_PER_PAGE;
  const selectedUsers = users.slice(startIndex, startIndex + USER_PER_PAGE);
  const rowws = selectedUsers.map((item) => (
    <TableRow
    key={item.Registration_Id}
    Registration_Id={item.Registration_Id}
    Attendee_Name={item.Attendee_Name}
    Attendee_Email={item.Attendee_Email}
    Status={item.Status}
    Ticket_Name={item.Ticket_Name}
    Order_Id={item.Order_Id}
    Title={item.Title}
    DOB={item.DOB}
    Gender={item.Gender}
    Contact_Number={item.Contact_Number}
    RACE_EVENT={item.RACE_EVENT}
    Address={item.Address}
    City={item.City}
    Pincode={item.Pincode}
    Emergency_Contact_Name={item.Emergency_Contact_Name}
    Emergency_Contact_Number={item.Emergency_Contact_Number}
    Emergency_Contact_Relation={item.Emergency_Contact_Relation}
    Blood_Group={item.Blood_Group}
    T_SHIRT_SIZE={item.T_SHIRT_SIZE}
    ADD_COUPON={item.ADD_COUPON}
    Attendee_Check_In={item.Attendee_Check_In}
    Check_In_Time={item.Check_In_Time}
    Attendee_Badge_Print={item.Attendee_Badge_Print}
    Badge_Print_Time={item.Badge_Print_Time}
    />
  ))
  return (
    <div>
      <section className={classes.tableSection}>
        <table className={`${classes.table} table table-hover`}>
          <thead>
            <tr>
              <th scope="col">Reg_Id</th>
              <th scope="col"> Attendee_Name</th>
              <th scope="col"> Email</th>
              <th scope="col">Status</th>
              <th scope="col">Ticket_Name</th>
              <th scope="col">Order_Id</th>
              <th scope="col">Title</th>
              <th scope="col">Date_Of_Birth</th>
              <th scope="col">Gender</th>
              <th scope="col">PhoneNumber</th>
              <th scope="col">RACE_EVENT</th>
              <th scope="col">Permanent_Address</th>
              <th scope="col">City</th>
              <th scope="col">Pincode</th>
              <th scope="col">E_ContactName</th>
              <th scope="col">E_ContactNumber</th>
              <th scope="col">E_ContactRelation</th>
              <th scope="col">Blood_Group</th>
              <th scope="col">T_SHIRT_SIZE</th>
              <th scope="col">ADD_COUPON</th>
              <th scope="col">Attendee_Check_In</th>
              <th scope="col">Check_In_Time</th>
              <th scope="col">Attendee_Badge_Print</th>
              <th scope="col">Badge_Print_Time</th>
            </tr>
          </thead>
          <tbody>{rowws}</tbody>
        </table>
      </section>
    </div>
  );
}

export default Users;
