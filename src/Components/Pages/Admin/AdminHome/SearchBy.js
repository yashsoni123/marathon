import React, { useState } from 'react'
import TableRow from './TableRow';
import classes from "./SearchBy.module.css";

const SearchBy = props => {
    const [data, setData] = useState([])
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [showTable, setShowTable] = useState(false)

    const onChangeHandler = (event) => {
        const { name, value } = event.target;
        if (name === "name") {
          setName(value);
        }
        if(name==="email"){
          setEmail(value);
        }
      };
    
      const onSearchHandler = (event) => {
        event.preventDefault();
        let filterData;
        if(email || name){
        filterData = props.data.filter((item) => name === item.Attendee_Name || email === item.Attendee_Email);
        }
        if(name && email){
            filterData = props.data.filter((item) => name === item.Attendee_Name && email === item.Attendee_Email);
        }
        if(filterData.length>0){
            setShowTable(true);
        }
        setData(filterData)
      };

    const filterData = data.map((item) => (
        <TableRow
          Registration_Id={item.Registration_Id}
          Attendee_Name={item.Attendee_Name}
          Attendee_Email={item.Attendee_Email}
          Status={item.Status}
          Ticket_Name={item.Ticket_Name}
          Order_Id={item.Order_Id}
          Title={item.Title}
          DOB={item.DOB}
          Gender={item.Gender}
          Contact_Number={item.Contact_Number}
          RACE_EVENT={item.RACE_EVENT}
          Address={item.Address}
          City={item.City}
          Pincode={item.Pincode}
          Emergency_Contact_Name={item.Emergency_Contact_Name}
          Emergency_Contact_Number={item.Emergency_Contact_Number}
          Emergency_Contact_Relation={item.Emergency_Contact_Relation}
          Blood_Group={item.Blood_Group}
          T_SHIRT_SIZE={item.T_SHIRT_SIZE}
          ADD_COUPON={item.ADD_COUPON}
          Attendee_Check_In={item.Attendee_Check_In}
          Check_In_Time={item.Check_In_Time}
          Attendee_Badge_Print={item.Attendee_Badge_Print}
          Badge_Print_Time={item.Badge_Print_Time}
        />
      ));

    return (
        <>
            <div className="row my-3">
            <div className="col-sm-4">
              <label htmlFor="name" className={classes.label}>Name</label>
              <input
                type="text"
                name="name"
                placeholder="Enter Name"
                className="form-control "
                onChange={onChangeHandler}
              />
            </div>
            <div className="col-sm-4">
              <label htmlFor="email" className={classes.label}>Email</label>
              <input
                type="email"
                name="email"
                placeholder="Enter Email"
                className="form-control"
                onChange={onChangeHandler}
              />
            </div>
            {/* <div className="col-sm-4">
              <label htmlFor="number">Phone Number</label>
              <input
                type="number"
                name="number"
                placeholder="Enter Phone Number"
                className="form-control"
              />
            </div> */}
            <div className="col-sm-4 mt-4 mx-auto">
              <button
                className="btn w-100 btn-primary"
                onClick={onSearchHandler}
              >
                Search
              </button>
            </div>
          </div>
          <div className="row px-4">
            {showTable && <section className={classes.tableSection}>
              <table className={`${classes.table} table table-hover`}>
                <thead>
                  <tr>
                    <th scope="col">Reg_Id</th>
                    <th scope="col"> Attendee_Name</th>
                    <th scope="col"> Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Ticket_Name</th>
                    <th scope="col">Order_Id</th>
                    <th scope="col">Title</th>
                    <th scope="col">Date_Of_Birth</th>
                    <th scope="col">Gender</th>
                    <th scope="col">PhoneNumber</th>
                    <th scope="col">RACE_EVENT</th>
                    <th scope="col">Permanent_Address</th>
                    <th scope="col">City</th>
                    <th scope="col">Pincode</th>
                    <th scope="col">E_ContactName</th>
                    <th scope="col">E_ContactNumber</th>
                    <th scope="col">E_ContactRelation</th>
                    <th scope="col">Blood_Group</th>
                    <th scope="col">T_SHIRT_SIZE</th>
                    <th scope="col">ADD_COUPON</th>
                    <th scope="col">Attendee_Check_In</th>
                    <th scope="col">Check_In_Time</th>
                    <th scope="col">Attendee_Badge_Print</th>
                    <th scope="col">Badge_Print_Time</th>
                  </tr>
                </thead>
                <tbody>{filterData}</tbody>
              </table>
            </section> }
          </div>
        </>
    )

};
export default SearchBy;