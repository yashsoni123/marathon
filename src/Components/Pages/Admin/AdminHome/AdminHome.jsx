import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { USER_PER_PAGE } from "../../../constraints";
import Pagination from "../../../Pagination";
import AdminNavbar from "../../../UI/AdminNavbar/AdminNavbar";
import Container from "../../../UI/Container/Container";
import Heading from "../../../UI/Heading/Heading";
import SmallText from "../../../UI/SmallText/SmallText";
import classes from "./AdminHome.module.css";
import SearchBy from "./SearchBy";
import TableRow from "./TableRow";
import Users from "./Users";

const AdminHome = () => {
  const [resData, setResData] = useState([]);
  const [tablerowww, setTablerowww] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [totalAttendees, setTotalAttendees] = useState(0);
  const [totalVolunteers, setTotalVolunteers] = useState(0);
  const authState = useSelector((state) => state);
  console.log("authState", authState);
  console.log("totalPAges", totalPages);
  const activePage = page;
  console.log();
  const getDetails = () => {
    axios.get("http://localhost:9008{rp/admin-home-attendees").then((res) => {
      setResData(res.data.result);
      setTotalPages(Math.ceil(res.data.result.length / USER_PER_PAGE));
      setTablerowww(
        res.data.result.map((item) => (
          <TableRow
            Registration_Id={item.Registration_Id}
            Attendee_Name={item.Attendee_Name}
            Attendee_Email={item.Attendee_Email}
            Status={item.Status}
            Ticket_Name={item.Ticket_Name}
            Order_Id={item.Order_Id}
            Title={item.Title}
            DOB={item.DOB}
            Gender={item.Gender}
            Contact_Number={item.Contact_Number}
            RACE_EVENT={item.RACE_EVENT}
            Address={item.Address}
            City={item.City}
            Pincode={item.Pincode}
            Emergency_Contact_Name={item.Emergency_Contact_Name}
            Emergency_Contact_Number={item.Emergency_Contact_Number}
            Emergency_Contact_Relation={item.Emergency_Contact_Relation}
            Blood_Group={item.Blood_Group}
            T_SHIRT_SIZE={item.T_SHIRT_SIZE}
            ADD_COUPON={item.ADD_COUPON}
            Attendee_Check_In={item.Attendee_Check_In}
            Check_In_Time={item.Check_In_Time}
            Attendee_Badge_Print={item.Attendee_Badge_Print}
            Badge_Print_Time={item.Badge_Print_Time}
          />
        ))
      );
    });

    axios.get("http://localhost:9008/total-attendees").then((res) => {
      setTotalAttendees(res.data.result[0].totalAttendees);
    });
    axios.get("http://localhost:9008/total-volunteers").then((res) => {
      setTotalVolunteers(res.data.result[0].totalVolunteers);
    });
  };

  const onClickHandler = (num) => {
    setPage(num);
  };

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <>
      <section>
        <AdminNavbar />
        <br />
        <br />
        <br />
        <br />
        <Container>
          <Heading>Admin Dashboard</Heading>
          <div className="row flex my-4">
            <div className={`${classes.sideBox} col-sm-3`}>
              <div>Total Attendees</div>
              <span>{totalAttendees}</span>
              <SmallText>Registered</SmallText>
            </div>
            <div className={`${classes.sideBox1} col-sm-4`}>
              <div>Total Events</div>
              <span className="my-auto">3</span>
              <SmallText>Registered</SmallText>
            </div>
            <div className={`${classes.sideBox2} col-sm-3 `}>
              <div>Total Volunteers</div>
              <span className="my-auto">{totalVolunteers}</span>
              <SmallText>Registered</SmallText>
            </div>
          </div>
          <br />
          <Heading>Search By</Heading>
          <SearchBy data={resData} />

          <Heading>Latest 10 Attendees Registered</Heading>
          <Users page={page} users={resData} />
          <div className={`${classes.paginationSection} col ms-auto`}>
            <Pagination
              totalPages={totalPages}
              onClickHandler={onClickHandler}
              activePage={activePage}
            />
          </div>
        </Container>
      </section>
    </>
  );
};

export default AdminHome;
