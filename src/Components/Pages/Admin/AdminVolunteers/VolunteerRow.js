import React from 'react'

const VolunteerRow = props => {
    return (
        <tr>
                      <td>{props.Registration_Id}</td>
                      <td><b>{props.Attendee_Name}</b></td>
                      <td><b>{props.Attendee_LastName}</b></td>
                      <td><b>{props.Attendee_Email}</b></td>
                      <td>{ props.age}</td>
                      <td>{ props.Gender}</td>
                      <td><b>{ props.Contact_Number}</b></td>
                      <td>{ props.Address}</td>
                      <td>{ props.City}</td>
                      <td>{ props.Pincode}</td>
                      <td>{ props.Emergency_Contact_Name}</td>
                      <td>{ props.Emergency_Contact_Number}</td>
                      <td>{ props.Blood_Group}</td>
                      <td>{ props.T_SHIRT_SIZE}</td>
                    </tr>
    )
};


export default VolunteerRow;