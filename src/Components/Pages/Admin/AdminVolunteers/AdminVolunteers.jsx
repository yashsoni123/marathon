import React, { useEffect, useState } from 'react'
import AdminNavbar from '../../../UI/AdminNavbar/AdminNavbar';
import Container from '../../../UI/Container/Container';
import Heading from '../../../UI/Heading/Heading';
import classes from "./AdminVolunteers.module.css";
import { withRouter } from 'react-router-dom/cjs/react-router-dom.min';
import axios from 'axios';
import VolunteerRow from './VolunteerRow';

const AdminVolunteers = () => {
  const [resData, setResData] = useState([])
  const [tablerowww, setTablerowww] = useState([]);
 useEffect(() => {
    axios.get("http://localhost:9008/admin-volunteers").then((res) => {
      setResData(res.data.result);
      console.log(res.data.result);
      setTablerowww(
        res.data.result.map((item) => (
          <VolunteerRow
          Registration_Id={item.volunteerID}
          Attendee_Name={item.firstname}
          Attendee_LastName={item.lastname}
          Attendee_Email={item.email}
          Title={item.Title}
          age={item.age}
          Gender={item.gender}
          Contact_Number={item.phoneNumber}
          Address={item.address}
          City={item.city}
          Pincode={item.zip}
          Emergency_Contact_Name={item.emergency_contact_name}
          Emergency_Contact_Number={item.emergency_contact_number}
          Blood_Group={item.blood_group}
          T_SHIRT_SIZE={item.t_shirt_size}
          />
        ))
      );
    });
  }, []);
    return (
        <div>
            <AdminNavbar />
            <section className='mt-5'>
                <Container>
                    <br />
                    <br />
                    <Heading>Registered Volunteers</Heading>
                    <section className={classes.tableSection}>

          <table className={`${classes.table} table table-hover`}>
              <thead>
                <tr>
                  <th scope="col">Volunteer_Id</th>
                  <th scope="col"> First_Name</th>
                  <th scope="col"> Last_Name</th>
                  <th scope="col"> Email</th>
                  <th scope="col"> Age</th>
                  <th scope="col">Gender</th>
                  <th scope="col">PhoneNumber</th>
                  <th  scope="col">Permanent_Address</th>
                  <th scope="col">City</th>
                  <th scope="col">Pincode</th>
                  <th scope="col">E_ContactName</th>
                  <th scope="col">E_ContactNumber</th>
                  <th scope="col">Blood_Group</th>
                  <th scope="col">T_SHIRT_SIZE</th>
                </tr>
              </thead>
              <tbody>
                  {tablerowww}
              </tbody>
            </table>
          </section>
                </Container>
            </section>
        </div>
    )
};

export default withRouter(AdminVolunteers);