import React, { useState } from "react";
import classes from "./AdminUpload.module.css";
import AdminNavbar from "../../../UI/AdminNavbar/AdminNavbar";
import Container from "../../../UI/Container/Container";
import Heading from "../../../UI/Heading/Heading";
import BoldText from "../../../UI/BoldText/BoldText";
import SmallText from "../../../UI/SmallText/SmallText";

const AdminUpload = () => {
  const [selectedFile, setSelectedFile] = useState();
  const [responseMsg, setResponseMsg] = useState("");
  const [statusMsg, setStatusMsg] = useState("")
  const [isDataUploaded, setIsDataUploaded] = useState(false);
  const [file, setFile] = useState("");

  const handleSubmission = (event) => {
    setIsDataUploaded(false);
    event.preventDefault();
    const formData = new FormData();

    formData.append("uploadfile", selectedFile);

    fetch("http://localhost:9008/admin-upload", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((result) => {
        setResponseMsg(result.message);
        setStatusMsg(result.status);

      })
      .then((res) => res.json())
      .then((res) => console.log("BLAA BLAA", res))
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const onChangeHandler = (event) => {
    console.log(event.target.files);
    setSelectedFile(event.target.files[0]);
  };
  return (
    <div>
      <AdminNavbar />
      <br />
      <br />
      <br />
      <br />
      
      <Container>
        <Heading>Upload Your Data here....</Heading>
        <section className={`${classes.uploadSection}`}>
          <div className="mb-3 col-sm-10 mx-auto">
            <div className={`${classes.mainBox}`}>

            <label htmFor="formFile" class="form-label my-3">
              <BoldText>Upload Your Attendees Data</BoldText>
            </label>
            {statusMsg && (
              <div className={`${classes.successResponse} my-3`}>
                <i className="fa fa-check-circle"></i> {responseMsg}
              </div>
            )}
            {responseMsg && !statusMsg  && (
              <div className={`${classes.alreadyResponse} my-3`}>
              <i className="fa  fa-user-times"></i> Something went Wrong
              </div>
            )}
            <span className={`${classes.fileicon} my-3`}><i className={`${classes.file2} fa fa-file-text-o`}></i></span>
            <SmallText>Upload Excel File Here........</SmallText>
            <form
              onSubmit={handleSubmission}
              encType="multipart/form-data"
              method="post"
              className="flex justify-content-between"
              >
              <input
                type="file"
                name="uploadfile"
                className={`${classes.uploadInput} btn text-center`}
                onChange={onChangeHandler}
                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                />
              <input type="submit" className={`${classes.uploadBtn} btn`} value="Upload File" />
            </form>
                </div>
          </div>
        </section>
      </Container>
    </div>
  );
};

export default AdminUpload;
