import React from 'react'
import Heading from '../../UI/Heading/Heading';
import classes from "./AboutSection.module.css";
// import card from "../../../Images/card1.jpg";
import logo from "../../../Images/logo.png";
import Container from '../../UI/Container/Container';
import SmallText from '../../UI/SmallText/SmallText';
import BoldText from '../../UI/BoldText/BoldText';

function AboutSection() {
    return (
        <Container>
            
        <section className={classes["about-marathon"]}>
          <Heading>About Bhiwadi Half Marathon</Heading>
          <br />
          <div className="flex row mx-auto">
            <div className=" col-lg-5 px-1">
              <img src={logo} className="img-fluid me-auto " alt="" />
            </div>
            <div className=" col-lg-7  ">
              <div>
                <p className={classes.aboutText}>
                  <SmallText><span className={classes.head3}>Bhiwadi Half Marathon</span> is India's
                  premiere endurance sports platform bringing Marathon , Running , Cycling events to sports fitness enthusiasts. <br /> OUR
                  PUPOSE is to provide sports enthusiasts a whole new experience
                  of "smashing" through both their physical and mental
                  limitations at various different terrains, thus entitling them
                  with the title –  <span className={classes.head3}>Bhiwadi Half Marathon</span>. We bring
                  those tough people out of their body, implant that toughness
                  in mind and help them discover their real tuff soul.
                  <span className={classes.head3}>~Bhiwadi Half Marathon</span>
                  </SmallText>
                </p>
              </div>
            </div>
          </div>
        </section>
        
        </Container>
    )
}

export default AboutSection;
