import React, { useState, useEffect } from "react";
import "./countdown.css";

const Countdown = () => {
  const temp = 1;

  const CountdownHandler = (startTme) => {
    const countdownDays = document.getElementById("countdownDays");
    const countdownHours = document.getElementById("countdownHours");
    const countdownMinutes = document.getElementById("countdownMinutes");
    const countdownSeconds = document.getElementById("countdownSeconds");
    const interval = setInterval(() => {
      const deadline = new Date(2022, 2, 27, 5);
      const current = new Date();
      const diff = deadline - current;
      const days = Math.floor(diff / (1000 * 60 * 60 * 24)) + "";
      const hours = Math.floor((diff / (1000 * 60 * 60)) % 24) + "";
      const minutes = Math.floor((diff / (1000 * 60)) % 60) + "";
      const seconds = Math.floor((diff / 1000) % 60) + "";
      countdownDays.innerHTML = `${days.length === 1 ? `0${days}` : days}`;
      countdownHours.innerHTML = `${hours.length === 1 ? `0${hours}` : hours}`;
      countdownMinutes.innerHTML = `${
        minutes.length === 1 ? `0${minutes}` : minutes
      }`;
      countdownSeconds.innerHTML = `${
        seconds.length === 1 ? `0${seconds}` : seconds
      }`;
    }, 1000);
  };

  useEffect(() => {
    CountdownHandler();
  }, [temp]);

  return (
    <div className="container-fluid h-100 w-100">
      <div className="countdown row col-sm-12">
        <div
          className="col-sm-2 col-xs-4 "
          id="countdownDays"
          data-content="Days"
        >
          0
        </div>
        <div
          className="col-sm-2 col-xs-4"
          id="countdownHours"
          data-content="Hours"
        >
          0
        </div>
        <div
          className="col-sm-2 col-xs-4"
          id="countdownMinutes"
          data-content="Minutes"
        >
          0
        </div>
        <div
          className="col-sm-2 col-xs-4"
          id="countdownSeconds"
          data-content="Seconds"
        >
          0
        </div>
      </div>
    </div>
  );
};

export default React.memo(Countdown);
