import React, { Fragment, useState } from "react";
import Header from "../../UI/Header/Header";
import classes from "./Home.module.css";
import BigButton from "../../UI/BigButton/BigButton";
import partner1 from "../../../Images/Home/Khelo.jpeg";
import partner2 from "../../../Images/Home/DAV.png";
import gold1 from "../../../Images/Home/Khandelwal.PNG";
import gold2 from "../../../Images/Home/bharatUltrasound.jpeg";
import gold3 from "../../../Images/Home/gold3.png";
import gold4 from "../../../Images/Home/gold4.png";
import s1 from "../../../Images/Home/support1.png";
import nahata from "../../../Images/nahata.jpeg";
import s2 from "../../../Images/Home/support2.png";
import s3 from "../../../Images/Home/support3.png";
import mediPar from "../../../Images/Home/03.jpg";
import mediaPar from "../../../Images/Home/mediaPar.png";
import enterPar from "../../../Images/Home/enterPar.png";
import DiaSponsor1 from "../../../Images/Home/DiaSponsor2.jpg";
import DiaSponsor2 from "../../../Images/Home/ecr2.jpeg";
import Heading from "../../UI/Heading/Heading";
import RaceCards from "../../UI/Cards/RaceCards/RaceCards";
import AboutSection from "./AboutSection";
import UpComingRaces from "./UpcomingRaces";
import { NavLink } from "react-router-dom";
import Countdown from "./countdown";
import Navbar from "../../UI/Navbar/Navbar";
import Container from "../../UI/Container/Container";
import SmallText from "../../UI/SmallText/SmallText";
import Modal from "./Modal";
import Footer from "../../UI/Footer/Footer";
import Certificate from "../PrizeMoney/Certificate";
import CarasoulSection from "../Media/CarasoulSection";
import PopUpModal from "./PopUpModal";
import { useHistory, useLocation } from "react-router-dom";

function Home() {
  // window.onload = function () {
  // document.getElementById("popUpModalBtn").click();
  // <PopUpModal />;
  // };
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const raceCards = UpComingRaces.map((item) => (
    <RaceCards
      title={item.title}
      key={item.id}
      location={item.location}
      date={item.date}
    />
  ));

  return (
    <Fragment>
      <button
        type="button"
        id="popUpModalBtn"
        className="btn btn-primary d-none"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
      ></button>
      {/* <Navbar /> */}
      <div className={classes.marginTopHome}></div>
      <Container>
        {/* <div className={classes.carasoulSection}> */}
        <CarasoulSection />
        {/* </div> */}
      </Container>

      <PopUpModal />
      <div className="col-10 mt-5 mx-auto">
        <Heading>Upcoming Marathon Begins in...</Heading>
        <br />
      </div>
      <section className={classes.countdownSection}>
        <div className="col-12 mx-auto">
          <Countdown />
        </div>
      </section>
      <br />
      <div className="col-10 mt-4 mx-auto">
        <Heading>UPCOMING RACES</Heading>
      </div>
      <br />
      <section className={`${classes["upcoming-races"]}  mt-3 col-sm-12`}>
        <div className="row col-10 flex mx-auto">{raceCards}</div>
        <br />
        <div className=" text-center mt-3">
          <BigButton>
            <NavLink className="text-decoration-none text-primary" to="/races">
              CHECK ALL RACES
            </NavLink>
          </BigButton>
        </div>
      </section>
      <AboutSection />
      <div className="col-10 mx-auto">
        <Heading>Get Your E-Certificate</Heading>
        <Certificate />
      </div>
      <section className={classes.sponsors}>
        <div className="col-10 mx-auto">
          <Heading>Organized By</Heading>
        </div>
        <Container>
          <div className={`${classes.organisedBy} row p-3`}>
            <div className="col-sm-4 col-md-5 mt-2 ">
              <img
                src={partner1}
                className="img-thumbnail p-3 border-0"
                alt="Partners"
              />
            </div>
            <div className="col-sm-4 col-md-5 mt-4 ms-auto   ">
              <img src={partner2} className="img-fluid" alt="Partners" />
            </div>
          </div>
        </Container>

        <div className="col-10 mx-auto">
          <Heading>Diamond Sponsors</Heading>
        </div>
        <div className="container-fluid my-5">
          <div className=" col-10 mx-auto">
            <div className="row">
              <div className="col-sm-5 col-md-6 my-1">
                <div className={classes.diamondSponsorSection1}>
                  <img
                    src={DiaSponsor1}
                    className="img-fluid"
                    alt="Greencole"
                  />
                  <img
                    src={DiaSponsor2}
                    className="img-fluid"
                    alt="ECR Buildtech Pvt Ltd"
                  />
                </div>
              </div>
              {/* <div className="col-sm-5 col-md-5 mt-4 ms-auto"></div> */}
              <div className="col-sm-5 col-md-5 mt-4 ms-auto">
                <img
                  src={gold1}
                  className="img-fluid"
                  alt="ECR Buildtech Pvt Ltd"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="col-10 mx-auto">
          <Heading>Golden Sponsors</Heading>
        </div>
        <Container>
          <div className="row my-3 center justify-content-around">
            <div className="col-sm-4">
              <img
                src={nahata}
                alt="Khandelwal Agencies"
                className="img-fluid my-3"
              />
            </div>
            <div className="col-sm-5">
              <img
                src={gold2}
                alt="Bharat Ultra Sound"
                className={`img-fluid my-4 ${classes.bharatImg}`}
              />
            </div>
          </div>
          <div className="row my-3 center justify-content-around">
            <div className="col-sm-4">
              <img
                src={gold3}
                alt="Perfect Nutrition"
                className="img-fluid my-2"
              />
            </div>
            <div className="col-sm-5">
              <img src={gold4} alt="Kamal Wires" className="img-fluid my-2" />
            </div>
          </div>
        </Container>

        <br />
        <Container>
          <div>
            <Heading>Medical Partner</Heading>
            <div className="mx-auto text-center my-5">
              <img
                src={mediPar}
                alt="Red Cross Society"
                className="img-fluid"
              />
            </div>
            <Heading>Media Partner</Heading>
            <div className="mx-auto text-center my-5">
              <img src={mediaPar} alt="90.8 Radio FM" className="img-fluid" />
            </div>
            <Heading>Entertainment Partner</Heading>
            <div className="mx-auto text-center my-5">
              <img src={enterPar} alt="PNP Events" className="img-fluid" />
            </div>
          </div>
        </Container>
        <div className="col-sm-11 mx-auto px-3">
          <Heading>Support By</Heading>
          <div className="container-fluid">
            <div className="col-lg-12 px-5">
              <div className="row center my-2 ">
                <div className="col-sm-2 my-4 col-md-3">
                  <img src={s1} alt="Voltas Big Shop" className="img-fluid" />
                </div>
                <div className="col-sm-2 my-4 col-md-3">
                  <img src={s2} alt="Rising Start" className="img-fluid" />
                </div>
                <div className="col-sm-2 my-4 col-md-3">
                  <img src={s3} alt="Dau Academy" className="img-fluid" />
                </div>
                <div className="col-sm-2 my-4 col-md-3">
                  <img src={s1} alt="" className="img-fluid" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
}

export default Home;
