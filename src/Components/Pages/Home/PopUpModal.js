import React from "react";
import reactDom, { createPortal } from "react-dom";
import img from "../../../Images/Home/POPUP1.jpeg"
import classes from "./PopUpModal.module.css";

const Modal = () => {
    return (
        <div className={`${classes.modalPopUp} modal fade row mt-3`} id="exampleModal"  aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog col-sm-10 ">
            <div className="modal-header">
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
              <img src={img} className="img-fluid" alt="" />
    
      
          </div>
      
      </div>
    )
}

const PopUpModal = () => {
    const portalElement = document.getElementById("overlays");
  return (
    <>
        {reactDom.createPortal(<Modal /> ,portalElement )}
    </>
  );
};

export default PopUpModal;
