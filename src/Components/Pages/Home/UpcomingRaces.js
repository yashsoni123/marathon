const UpComingRaces = [
  {
    id: "r1",
    date: "27-Mar-2022",
    location: "Bhiwadi",
    title: "FUN RUN (3KM)",
  },
  {
    id: "r2",
    date: "27-Mar-2022",
    location: "Bhiwadi",
    title: "BEGINNER'S RUN (5KM)",
  },
  {
    id: "r3",
    date: "27-Mar-2022",
    location: "Bhiwadi",
    title: "PRO RUN (10KM)",
  },
  {
    id: "r4",
    date: "27-Mar-2022",
    location: "Bhiwadi",
    title: "ULTRA RUN (21KM)",
  },
];

export default UpComingRaces;
