import React from "react";
import SmallText from "../../UI/SmallText/SmallText";
import classes from "./Modal.module.css";

const Modal = props => {
  return (
    <section>
      <div
        className="modal fade"
        id="staticBackdrop"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabindex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className={`${classes.modal} modal-dialog modal-lg mt-5`}>
          <div className="modal-content mt-5">
            <div className="modal-header">
              <h5 className="modal-title h4" id="staticBackdropLabel">
                <b>{props.title}</b>
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <SmallText>
                {props.para}
              </SmallText>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Modal;
