import {  PDFDocument , rgb  } from "pdf-lib";
import fontkit  from "@pdf-lib/fontkit"
import certificate from "../../../Images/Certificate/certificate.pdf";
import fontStyle from "../../../Images/Certificate/OpenSans-Bold.ttf";

const LoadPDF = (props) => {
  
    async function createPdf(name) {
        const exBytes = await fetch(certificate).then((res) => {
            return res.arrayBuffer();
        }) ;
        const exFont = await fetch(fontStyle).then((res) => {
            return res.arrayBuffer();
        })
        
    const pdfDoc = await PDFDocument.load(exBytes);
    pdfDoc.registerFontkit(fontkit);
    const myFont = await pdfDoc.embedFont(exFont);

    const pages = pdfDoc.getPages();
    const firstPg = pages[0];

        firstPg.drawText(name,{
            x: 110,
            y: 250,
            size: 45,
            font: myFont,
            color: rgb(0 , 0.84, 0.67)
       })
   
       const uri = await pdfDoc.saveAsBase64({dataUri: true});
       document.getElementById("myPdf").src = uri;
      }

      createPdf(props.name)
    
    return (
      <><iframe title="Certificate" src="" id="myPdf" height="400px" width="100%"  type="application/pdf" /></>
    );
  };
  
  export default LoadPDF;