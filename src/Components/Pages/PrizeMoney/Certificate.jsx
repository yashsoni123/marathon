import axios from "axios";
import React, { useEffect, useState } from "react";
import useInputForm from "../../Custom Hooks/useInputForm";
import classes from "./Certificate.module.css";
import LoadPDF from "./LoadPDF";

function Certificate() {
  const [showCertificate, setShowCertificate] = useState(false);
  const [userName, setUserName] = useState("");
  const [resMsg, setResMsg] = useState("");
  const [newDate, setNewDate] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [formError, setFormError] = useState({});
  const [formIsValid, setFormIsValid] = useState(false);
  const [data, setData] = useState({
    email: "",
    number:"",
    date:""
  });
  const [response, setResponse] = useState([]);

  useEffect(()=>{
    setFormIsValid(Object.keys(formError).length ===0);
    if(Object.keys(formError).length === 0 && isSubmit){
    }
  },[formError]);

  const onClickHandler = (e) => {
    e.preventDefault();
    setIsSubmit(true);
    setShowCertificate(false);
    
    setTimeout(() => {
      if(formIsValid){
        axios.post("http://localhost:9008/prize-money", data).then((res) => {
          console.log(res);
          setResponse(res.data);
          if (res.data.status) {
            const fullName = `${res.data.result[0].Attendee_Name} `;
            setUserName(fullName);
          } else {
            setResMsg(res.data.message);
          }
        });
      setShowCertificate(true);
      setData({
        email: "",
        number:"",
        date:""
      })
      document.getElementById('certificatedate').value = "";
      }
    }, 500);
  };

    const validate = values => {
      const errors = {};
      const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i;
      if(!values.email){
        errors.email="Email is Required*"
      } else if(!regex.test(values.email)){
        errors.email="This is not Valid Email Format*";
      }
      if(!values.number){
        errors.number="Number is Required*"
      }else if(values.number.length !== 10){
        errors.number="This is not Valid Phone Number*"
      }
      if(!values.date){
        errors.date="Date is Required*"
      }
      return errors;
    }

  const onBlurHandler = () => {
    setFormIsValid(Object.keys(formError).length ===0);
    setFormError(validate(data));
  }
  const onChangeHandler = (event) => {
    const {name , value} = event.target;
    console.log(name,value);
    let datee;
    if(name === "date"){
      const temp = new Date(value);
      const dateMonth = temp.getMonth() < 10 ? `0${temp.getMonth() + 1}`: temp.getMonth() + 1;
      const dateDate = temp.getDate() < 10 ? `0${temp.getDate()}`: temp.getDate();
      const dateYear = temp.getFullYear();
      datee = `${dateDate}-${dateMonth}-${dateYear}`;
      setNewDate(datee);
      setData((prevValue) => {
        return {
          ...prevValue,
          [name] : datee
        }
      });
    }
    if (name === "email") {
      setData((prevValue) => {
        return {
          ...prevValue,
          [name] : value
        }
      });
    }
    if(name === "number"){
      setData((prevValue) => {
        return {
          ...prevValue,
          [name] : value
        }
      });
    }
  };

  return (
    <section>
        { showCertificate && !response.status  && (
      <div className="col-10  mx-auto">
          <div className={`${classes.errResponse} my-3`}>
            <i className="fa fa-minus-circle"></i> {resMsg}
          </div>
      </div>
        )}
      <form className="row g-3 mt-3 flex col-10 mx-auto">
        <div className="col-md-4">
          <label htmlFor=" email" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control invalid"
            id=" email"
            name="email"
            onBlur={onBlurHandler}
            value={data.email}
            placeholder="Enter your Email"
            onChange={onChangeHandler}
          />
        { formError.email && <small id="emailNotValid" class=" ms-2 form-text text-danger">{formError.email}</small>}
        </div>
        <div className="col-md-4">
          <label htmlFor=" email" className="form-label">
            Phone Number
          </label>
          <input
            type="number"
            className="form-control"
            id=" email"
            name="number"
            onBlur={onBlurHandler}
            value={data.number}
            placeholder="Enter your Phone Number"
            onChange={onChangeHandler}
          />

        { formError.number && <small id="emailNotValid" class=" ms-2 form-text text-danger">{formError.number}</small>}
        </div>
        <div className="col-md-4">
          <label htmlFor=" email" className="form-label">
            Date Of Birth
          </label>
          <input
            type="date"
            className="form-control"
            id="certificatedate"
            name="date"
            onBlur={onBlurHandler}
            placeholder="Enter your DOB"
            onChange={onChangeHandler}
          />
          { formError.date && <small id="emailNotValid" class=" ms-2 form-text text-danger">{formError.date}</small>}
        </div>
        <div className="col-md-6 flex">
          <button
            onClick={onClickHandler}
            className="btn mt-3 btn-primary mx-auto"
          >
            Get Certificate
          </button>
        </div>
      </form>
      <br />

      {showCertificate && response.status && <LoadPDF name={userName} />}
    </section>
  );
}

export default Certificate;
