import React from "react";
import Footer from "../../UI/Footer/Footer";
import classes from "./PrizeCard.module.css";

const PrizeCard = (props) => {
  return (
      <div className={`${classes["prize-card"]} col-sm-6  mx-auto  col-md-4`}>
      <div className={`${classes.box} `}>
      <div className={`${classes["main-heading"]} text-center`}>{props.position}</div>
        <div className="row center">
        <div className="col-md-5  center col-sm-12">
          <img src={props.img} height="170px" className="img-fluhhid" alt="" />
        </div>
        <div className="col-md-7 col-sm-12 mt-2 text-center">
          <div className={classes["race-heading"]}>
            <span className={classes.darkText}>Race: </span> {props.type}
          </div>
          <div className={classes["gender-heading"]}>
            <span className={classes.darkText}>For: </span> {props.gender}
          </div>
          <div className={classes["prize-heading"]}>
            <span className={classes.darkText}>Prize Money: </span>₹
            {props.prizeMoney}
          </div>
        </div>
        </div>
      </div>
    </div>
    
  );
};

export default PrizeCard;
