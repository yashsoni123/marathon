import React, { useState } from "react";
import "./PrizeMoney.css";
import Heading from "../../UI/Heading/Heading";
import PrizeCard from "./PrizeCard";
import prizeData from "./prizeData";
import Certificate from "./Certificate";
import Navbar from "../../UI/Navbar/Navbar";
import Footer from "../../UI/Footer/Footer";

function PrizeMoney() {
  const [data, setData] = useState(prizeData);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const filterCard = (type) => {
    if (type === "MALE") {
      const filterData = prizeData.filter((item) => item.gender === "MALE");
      setData(filterData);
    }
    if (type === "FEMALE") {
      const filterData = prizeData.filter((item) => item.gender === "FEMALE");
      setData(filterData);
    }
  };
  return (
    <div>
      {/* <Navbar /> */}
      <section className="prize-section">
        <div className="col-11 mx-auto">
          <Heading>Prizes</Heading>
        </div>
        <div className="col-10 mx-auto">
          <div className="text-center filter_section ">
            Search by Gender
            <button
              onClick={filterCard.bind(null, "MALE")}
              className=" mx-2 btn"
            >
              <i className=" fa fa-male "></i> Male
            </button>
            <button
              onClick={filterCard.bind(null, "FEMALE")}
              className=" mx-2 btn"
            >
              <i className=" fa fa-female "></i> Female
            </button>
            <button onClick={() => setData(prizeData)} className=" mx-2 btn">
              ALL
            </button>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row col-11 mx-auto">
            {data.map((item) => (
              <PrizeCard
                key={item.id}
                position={item.position}
                img={item.img}
                prizeMoney={item.prizeMoney}
                gender={item.gender}
                type={item.type}
              />
            ))}
          </div>
          <br />
        </div>
      </section>
    </div>
  );
}

export default PrizeMoney;
