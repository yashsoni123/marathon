import React from "react";
import RaceCards from "../../UI/Cards/RaceCards/RaceCards";
import Container from "../../UI/Container/Container";
import Header from "../../UI/Header/Header";
import classes from "./Races.module.css";
import Heading from "../../UI/Heading/Heading";
import UpComingRaces from "../Home/UpcomingRaces";
import Navbar from "../../UI/Navbar/Navbar";

function Races() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const raceCards = UpComingRaces.map((item) => (
    <RaceCards
      key={item.id}
      title={item.title}
      location={item.location}
      date={item.date}
    />
  ));

  return (
    <>
      {/* <Navbar /> */}
      <header className={`${classes.header} flex`}>
        <Container>
          <div className={`${classes.mainText} `}>
            <div className={`${classes.redText} d-flex`}>
              {" "}
              THE &nbsp;<span className={classes.blueText}> MARATHON</span>{" "}
            </div>
            <div className={`${classes.redText} d-flex`}>
              {" "}
              CAN &nbsp; <span className={classes.blueText}> HUMBLE</span>{" "}
            </div>
            <div className={`${classes.redText} d-flex`}>
              {" "}
              YOU.{" "}
              <span className={classes.blueText}>
                <i className="fa fa-smile-o"></i>{" "}
              </span>{" "}
            </div>
          </div>
        </Container>
      </header>
      <br />
      <Container>
        <Heading>EVENTS CALENDER</Heading>
      </Container>
      <br />
      <div className={classes.racesBG}>
        <Container>
          <div className="row center mt-5">{raceCards}</div>
          <br />
          <Heading>EVENTS HISTORY</Heading>
          <div className="row center mt-5">{raceCards}</div>

          <br />
        </Container>
      </div>
    </>
  );
}

export default React.memo(Races);
