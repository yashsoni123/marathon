import React, { useEffect } from "react";
import classes from "./VolunteerRegistration.module.css";
import { useState } from "react";
import Heading from "../../UI/Heading/Heading";
import axios from "axios";
import Navbar from "../../UI/Navbar/Navbar";

const VolunteerRegistration = () => {
  const [response, setResponse] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [formErrors, setFormErrors] = useState({});
  const [formIsValid, setFormIsValid] = useState(false);
  const [data, setData] = useState({
    firstname: "",
    lastname: "",
    email: "",
    phoneNumber: "",
    state: "",
    zip: "",
    address: "",
    gender: "",
    age: "",
    city: "",
    emeName: "",
    emeNumber: "",
    bloodGroup: "",
  });

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const onClickHandler = (event) => {
    event.preventDefault();
    setIsSubmit(true);
    setFormErrors(validate(data));
    if (formIsValid) {
      axios
        .post("http://localhost:9008/volunteer/registration", data)
        .then((res) => {
          setResponse(res.data.message);
        });
      setData({
        firstname: "",
        lastname: "",
        email: "",
        phoneNumber: "",
        state: "",
        zip: "",
        address: "",
        gender: "",
        age: "",
        city: "",
        emeName: "",
        emeNumber: "",
        bloodGroup: "",
      });
    }
  };
  const onBlurHandler = () => {
    setFormIsValid(Object.keys(formErrors).length === 0);
    setFormErrors(validate(data));
  };

  useEffect(() => {
    console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
    }
    setFormIsValid(Object.keys(formErrors).length === 0);
  }, [formErrors]);

  const validate = (values) => {
    const errors = {};
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i;
    if (!values.firstname) {
      errors.firstname = "First Name is Required*";
    }
    if (!values.lastname) {
      errors.lastname = "Last Name is Required*";
    }
    if (!values.age) {
      errors.age = "Age Is Required*";
    } else if (+values.age < 18) {
      errors.age = "Sorry You Must be more than 18 Years old*";
    }
    if (!values.phoneNumber) {
      errors.phoneNumber = "Phone Number is Required*";
    } else if (values.phoneNumber.length !== 10) {
      errors.phoneNumber = "Phone Number Must be Valid*";
    }
    if (!values.state) {
      errors.state = "state is Required*";
    }
    if (!values.city) {
      errors.city = "City is Required*";
    }
    if (!values.emeName) {
      errors.emeName = "Emergency Contact Name is Required*";
    }
    if (!values.emeNumber) {
      errors.emeNumber = "Emergency Contact Number is Required*";
    } else if (values.emeNumber.length !== 10) {
      errors.emeNumber = "Emergency Contact Number must be Valid*";
    }
    if (!values.bloodGroup) {
      errors.bloodGroup = "Blood Group is Required*";
    }
    if (!values.gender) {
      errors.gender = "Gender is Required*";
    }
    if (!values.tShirtSize) {
      errors.tShirtSize = "T-Shirt Size is Required*";
    }
    if (!values.address) {
      errors.address = "Address is Required*";
    } else if (values.address.length < 7) {
      errors.address = "Complete Address is Required*";
    }
    if (!values.zip) {
      errors.zip = "Zip is Required*";
    } else if (values.zip.length !== 6) {
      errors.zip = "Zip is not Valid*";
    }
    if (!values.email) {
      errors.email = "Email is Required*";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not a Valid Email Format*";
    }
    return errors;
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  return (
    <section className={classes.main}>
      {/* <Navbar /> */}
      <main className="col-md-8 col-sm-10 col-xs-12 mt-5 px-5 mx-auto">
        <Heading>Volunteer Registration</Heading>
        <br />
        {response === "Succesfully Registered" && (
          <div className={`${classes.successResponse} my-3`}>
            <i className="fa fa-check-circle"></i> {response}
          </div>
        )}
        {response === "Something Went Wrong" && (
          <div className={`${classes.errorResponse} my-3`}>
            <i className="fa  fa-minus-circle"></i> {response}
          </div>
        )}
        {response === "User alerady registered" && (
          <div className={`${classes.alreadyResponse} my-3`}>
            <i className="fa  fa-user-times"></i> {response}
          </div>
        )}
        <form className="row g-3">
          <div className="col-md-6">
            <label htmlFor="fistName" className="form-label">
              First Name
            </label>
            <input
              type="text"
              className="form-control"
              name="firstname"
              id="fistName"
              onBlur={onBlurHandler}
              value={data.firstname}
              placeholder="Enter your First Name"
              onChange={onChangeHandler}
            />
            {formErrors.firstname && (
              <small id="firstName" class="form-text ms-1 text-danger">
                {formErrors.firstname}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor="lastName" className="form-label">
              Last Name
            </label>
            <input
              type="text"
              className="form-control"
              id="lastName"
              onBlur={onBlurHandler}
              name="lastname"
              value={data.lastname}
              placeholder="Enter your Last Name"
              onChange={onChangeHandler}
            />
            {formErrors.lastname && (
              <small id="lastname Error" class="form-text ms-1 text-danger">
                {formErrors.lastname}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor=" PhoneNumber" className="form-label">
              Phone Number
            </label>
            <input
              type="number"
              className="form-control"
              name="phoneNumber"
              onBlur={onBlurHandler}
              value={data.phoneNumber}
              id="PhoneNumber"
              placeholder="Enter your Phone Number"
              onChange={onChangeHandler}
            />
            {formErrors.phoneNumber && (
              <small id="phoneNumberError" class="form-text ms-1 text-danger">
                {formErrors.phoneNumber}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor=" age" className="form-label">
              Age
            </label>
            <input
              type="number"
              className="form-control"
              id=" age"
              name="age"
              onBlur={onBlurHandler}
              value={data.age}
              placeholder="Enter your Age"
              onChange={onChangeHandler}
            />
            {formErrors.age && (
              <small id="firstName" class="form-text ms-1 text-danger">
                {formErrors.age}
              </small>
            )}
          </div>

          <div className="col-md-6">
            <label htmlFor="email" className="form-label">
              Email
            </label>
            <input
              type="email"
              onBlur={onBlurHandler}
              className="form-control"
              id="email"
              name="email"
              value={data.email}
              placeholder="Enter your Email"
              onChange={onChangeHandler}
            />
            {formErrors.email && (
              <small id="firstName" class="form-text ms-1 text-danger">
                {formErrors.email}
              </small>
            )}
          </div>
          <div className="  col-md-6">
            <label htmlFor="gender" className="form-label">
              Gender
            </label>

            <div className="d-flex ">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="gender"
                  value="MALE"
                  id="flexRadioDefault1"
                  onChange={onChangeHandler}
                />
                <label className="form-check-label" htmlFor="flexRadioDefault1">
                  Male
                </label>
              </div>
              <div className="form-check">
                <input
                  className=" ms-3 form-check-input"
                  type="radio"
                  name="gender"
                  value="FEMALE"
                  id="flexRadioDefault2"
                  onChange={onChangeHandler}
                />
                <label
                  className=" ms-1 form-check-label"
                  htmlFor="flexRadioDefault2"
                >
                  Female
                </label>
              </div>
            </div>
            {formErrors.gender && (
              <small id="genderError" class="form-text ms-1 text-danger">
                {formErrors.gender}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor="email" className="form-label">
              Emergency Contact Name
            </label>
            <input
              type="text"
              className="form-control"
              onBlur={onBlurHandler}
              id="emeNameInput"
              name="emeName"
              value={data.emeName}
              placeholder="Enter your Emergency Contact Name"
              onChange={onChangeHandler}
            />
            {formErrors.emeName && (
              <small id="emeNameInputError" class="form-text ms-1 text-danger">
                {formErrors.emeName}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor="emeNumberInput" className="form-label">
              Emergency Contact Number
            </label>
            <input
              type="number"
              className="form-control"
              onBlur={onBlurHandler}
              id="emeNumberInput"
              name="emeNumber"
              value={data.emeNumber}
              placeholder="Enter your Emergency Contact Number"
              onChange={onChangeHandler}
            />
            {formErrors.emeNumber && (
              <small id="firstName" class="form-text ms-1 text-danger">
                {formErrors.emeNumber}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor="bloodGroup" className="form-label">
              Blood Group
            </label>
            <input
              type="text"
              className="form-control"
              id="bloodGroup"
              name="bloodGroup"
              onBlur={onBlurHandler}
              value={data.bloodGroup}
              placeholder="Enter your Blooad Group"
              onChange={onChangeHandler}
            />
            {formErrors.bloodGroup && (
              <small id="bloodGroup error" class="form-text ms-1 text-danger">
                {formErrors.bloodGroup}
              </small>
            )}
          </div>
          <div className="  col-md-6">
            <label htmlFor="tShirtSize" className="form-label">
              T-Shirt Size
            </label>

            <div className="d-flex">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tShirtSize"
                  value="M"
                  id="flexRadioDefault1"
                  onChange={onChangeHandler}
                />
                <label className="form-check-label" htmlFor="flexRadioDefault1">
                  M
                </label>
              </div>
              <div className="form-check">
                <input
                  className=" ms-3 form-check-input"
                  type="radio"
                  name="tShirtSize"
                  value="L"
                  id="flexRadioDefault2"
                  onChange={onChangeHandler}
                />
                <label className=" ms-1 form-check-label" htmlFor="tShirtSize">
                  L
                </label>
              </div>
              <div className="form-check">
                <input
                  className=" ms-3 form-check-input"
                  type="radio"
                  name="tShirtSize"
                  value="XL"
                  id="flexRadioDefault2"
                  onChange={onChangeHandler}
                />
                <label className=" ms-1 form-check-label" htmlFor="tShirtSize">
                  XL
                </label>
              </div>
            </div>
            {formErrors.tShirtSize && (
              <small id="tShirtSize error" class="form-text ms-1 text-danger">
                {formErrors.tShirtSize}
              </small>
            )}
          </div>

          <div className="col-12">
            <label htmlFor="Address" className="form-label">
              Address
            </label>
            <input
              type="text"
              className="form-control"
              onBlur={onBlurHandler}
              id="Address"
              name="address"
              value={data.address}
              placeholder="1234 Area Street Name"
              onChange={onChangeHandler}
            />
            {formErrors.address && (
              <small id="address Error" class="form-text ms-1 text-danger">
                {formErrors.address}
              </small>
            )}
          </div>
          <div className="col-md-6">
            <label htmlFor="City" className="form-label">
              City
            </label>
            <input
              type="text"
              className="form-control"
              onBlur={onBlurHandler}
              value={data.city}
              name="city"
              onChange={onChangeHandler}
              id="City"
              placeholder="Enter your City"
            />
            {formErrors.city && (
              <small id="city error" class="form-text ms-1 text-danger">
                {formErrors.city}
              </small>
            )}
          </div>
          <div className="col-md-4">
            <label htmlFor="State" className="form-label">
              State
            </label>
            <select
              id="State"
              name="state"
              value={data.value}
              onBlur={onBlurHandler}
              className="form-select"
              onChange={onChangeHandler}
            >
              <option>Choose...</option>
              <option>Rajasthan</option>
              <option>Punjab</option>
              <option>Maharastra</option>
            </select>
            {formErrors.state && (
              <small id="state Error" class="form-text ms-1 text-danger">
                {formErrors.state}
              </small>
            )}
          </div>
          <div className="col-md-2">
            <label htmlFor="Zip" className="form-label">
              ZipCode
            </label>
            <input
              type="text"
              name="zip"
              onBlur={onBlurHandler}
              onChange={onChangeHandler}
              value={data.zip}
              placeholder="PinCode"
              className="form-control"
              id="Zip"
            />
            {formErrors.zip && (
              <small id="PinCode Error" class="form-text ms-1 text-danger">
                {formErrors.zip}
              </small>
            )}
          </div>

          <div className="col-12 mt-4 text-center ">
            <button
              type="button"
              onClick={onClickHandler}
              className="mx-auto text-center border-0 col-6 btn btn-primary"
            >
              {formIsValid ? "Register" : "Check All Details"}
            </button>
          </div>
        </form>
      </main>
      <br />
    </section>
  );
};

export default VolunteerRegistration;
