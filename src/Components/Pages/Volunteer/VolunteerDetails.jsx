import React from "react";
import Container from "../../UI/Container/Container";
import Heading from "../../UI/Heading/Heading";
import Navbar from "../../UI/Navbar/Navbar";
import SmallText from "../../UI/SmallText/SmallText";
import classes from "./VolunteerDetails.module.css";

function VolunteerDetails() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div>
      <section className={classes.volterDetails}>
        <Container>
          <Heading>OPPORTUNITIES</Heading>
          <div className="mt-2 px-2">
            <SmallText>
              All Marathons rely heavily on volunteers and helping out takes
              your love of running to another level. “Each volunteer’s passion
              for the sport takes on a new dimension when they help other
              runners accomplish their goals
            </SmallText>

            <SmallText>
              We are looking at a dedicated team of volunteers with team spirit
              who are willing to work long hours during the week prior to the
              Marathon and on the day of the
            </SmallText>

            <SmallText>
              There are plenty of opportunities to suit whatever experience you
              are looking at{" "}
            </SmallText>

            <SmallText>
              Volunteers will have to make their own travel and accommodation
              arrangements. Food will be provided during the volunteering period
              and Certificates will be given as a token of Appreciation.
            </SmallText>
          </div>
          <br />
          <Heading>SUPPORT</Heading>
          <section className="my-4 px-2">
            <SmallText>
              As Bhiwadi Half Marathon is still in its initial stages, we
              welcome any form of support from organisations and experienced
              runners – coaches, as well as professionals – medical support,
              first aid, doctors, setting up aid and snack stations,
              physiotherapists.
            </SmallText>
          </section>
        </Container>
      </section>
    </div>
  );
}

export default VolunteerDetails;
