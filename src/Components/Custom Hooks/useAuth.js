import React , {useCallback, useState} from 'react'


const useAuth = (intialValue) => {
    const [isAuth, setIsAuth] = useState(intialValue);
    const login = () => {

        setIsAuth(true);
    };
    const logout = () => {
        setTimeout(() => {
            setIsAuth(false);
        }, 500);
    };
    return [isAuth , login , logout]
};

export default useAuth;