import { useReducer } from "react";


const defaultInputState = {
    value:"",
    isTouched:false
}

const inputStateReducer = (state , action) => {

    if(action.type === "ADD"){
        return {  value:action.value , isTouched: state.isTouced }
    }
    if(action.type === "BLUR"){
        return {  value: state.value , isTouched: true  }
    }
    if(action.type === "RESET"){
        return {  value: '', isTouched: false  }
    }

    return {
        value:"",
        isTouched:false
    }
}

const useInputForm = (validateValue) => {

    const [inputState, dispatchState] = useReducer(inputStateReducer, defaultInputState);

    const valueChangeHandler = (event) => {
        dispatchState({type:"ADD",value:event.target.value});
    }

    const isValid = validateValue(InputEvent.isValid)
    const hasError = !isValid && inputState.isTouched;
    const inputBlurHandler = () => {
        dispatchState({type:"BLUR"})
    };
    const reset = () => {
        dispatchState({type:"RESET"})
    };
    return {
        value: inputState.value,
        isValid,
        hasError,
        valueChangeHandler,
        inputBlurHandler,
        reset,
    }
};

export default useInputForm;