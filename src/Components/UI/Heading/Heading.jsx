import React from 'react';
import classes from "./Heading.module.css";


function Heading(props) {
    return (
        <div className={classes.head1}>{props.children}</div>
    )
}

export default Heading
