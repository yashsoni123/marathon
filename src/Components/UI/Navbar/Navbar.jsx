import React from "react";
import classes from "./Navbar.module.css";
import { NavLink } from "react-router-dom";
import img1 from "../../../Images/prizeImg.png";
import logo from "../../../Images/logo.png";

const Navbar = () => {
  // if (window.matchMedia("(max-width: 991px)").matches) {
  //   const navLinks = document.querySelectorAll(".nav-item:not(.dropdown)");
  //   const menuToggle = document.getElementById("navbarToggler");
  //   const bsCollapse = new bootstrap.Collapse(menuToggle, { toggle: false });
  //   navLinks.forEach((l) => {
  //     l.addEventListener("click", () => {
  //       bsCollapse.toggle();
  //     });
  //   });
  // }

  const toggleNavbar = () => {
    console.log("Hello");
    document.getElementById("navToggleBtn").click();
  };

  return (
    <>
      <nav
        className={`navbar navbar-expand-lg flex-lg-column  navbar-dark bg-dark  text-center fixed-top ${classes.bg_blue}`}
      >
        {/* <marquee className={`${classes.marqueenText} center mx-auto`} width="80%" scrollamount="3" direction="left" height="25px">
          <span className="badge rounded-pill bg-warning text-dark">Important  &nbsp;<i className="fa  fa-bullhorn"></i> </span>
          &nbsp;The Event on 16 Jan 2022 is <span className="text-white fw-bold">POSTPONED</span> due to Covid19. Till Further Order By Rajasthan Govt. So Stay Safe Stay Fit.
          <span className="badge rounded-pill bg-warning text-dark">Announcement &nbsp; <i className="fa  fa-bullhorn"></i></span>
        </marquee> */}
        <div className="container-fluid flex-lg-column ">
          <div className={` navbar-brand`}>
            <NavLink
              className={`${classes["navbar-brand"]}  ms-2 mt-2 flex`}
              to="/"
            >
              <img src={logo} className={classes.logo} alt="" />
            </NavLink>
          </div>
          <button
            className="navbar-toggler"
            type="button"
            id="navToggleBtn"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav flex me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <div className="nav-link " aria-current="page">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/home"
                    onClick={() => toggleNavbar()}
                  >
                    Home
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/about-us"
                    onClick={() => toggleNavbar()}
                  >
                    About Us
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/general-info"
                  >
                    General-Info
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/races"
                    onClick={() => toggleNavbar()}
                  >
                    Events
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]} mx-2`}
                    to="/registration"
                    onClick={() => toggleNavbar()}
                  >
                    Registration
                  </NavLink>
                </div>
              </li>
              <li className="nav-item dropdown ">
                <div
                  className={` dropdown-toggle ${classes["nav-link"]} px-1`}
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  onClick={() => toggleNavbar()}
                >
                  Volunteer
                </div>
                <ul
                  className="dropdown-menu bg-secondary text-center"
                  aria-labelledby="navbarDropdown"
                >
                  <li>
                    <div className="nav-link ">
                      <NavLink
                        className={`${classes["nav-dropdown-link"]} boredr-0`}
                        to="/volunteer/details"
                        onClick={() => toggleNavbar()}
                      >
                        Details
                      </NavLink>
                    </div>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <div className="nav-link ">
                      <NavLink
                        className={`${classes["nav-dropdown-link"]}`}
                        to="/volunteer/registration"
                        onClick={() => toggleNavbar()}
                      >
                        Registration
                      </NavLink>
                    </div>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/prize-money"
                    onClick={() => toggleNavbar()}
                  >
                    Prizes
                    <img
                      src={img1}
                      height="45px"
                      className={classes.wobble}
                      alt=""
                    />
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/media"
                    onClick={() => toggleNavbar()}
                  >
                    Media
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <div className="nav-link">
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/admin-login"
                    onClick={() => toggleNavbar()}
                  >
                    Admin
                  </NavLink>
                </div>
              </li>
          <li className="nav-item">
                <div className="nav-link">
                  {" "}
                  <NavLink
                    className={`${classes["nav-link"]}`}
                    to="/contact-us"
                    onClick={() => toggleNavbar()}
                  >
                    Contact Us
                  </NavLink>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
