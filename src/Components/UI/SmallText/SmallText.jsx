import React from 'react';
import classes from "./SmallText.module.css";

function SmallText(props) {
    return (
        <p className={`${classes.smallText}`}>{props.children}</p>
    )
}

export default SmallText;
