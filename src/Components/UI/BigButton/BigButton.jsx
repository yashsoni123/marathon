import React from 'react';
import classes from "./BigButton.module.css";

function BigButton(props) {
    return (
        <button className={`${classes.btn} text-white`}>
            {props.children}
        </button>
    )
}

export default BigButton;
