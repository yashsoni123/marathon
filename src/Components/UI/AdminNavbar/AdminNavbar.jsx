import React  from "react";
import classes from "./AdminNavbar.module.css";
import { useHistory } from "react-router-dom";
import logo from "../../../Images/logo.png";
import { NavLink } from "react-router-dom";
import { useCookies } from "react-cookie";

const AdminNavbar = () => {
  const history = useHistory();
  const [cookies , setCookies , removeCookie] = useCookies();
  
  const onLogoutHandler = () => {
    removeCookie("bhiwadiHalfMarathon");
    history.push('/home');

  }
  return (
    <>
 <nav className={`navbar navbar-expand-lg flex navbar-light text-center fixed-top ${classes.bg_blue} ${classes.navBar} `}>
  <div className="container-fluid">
    <a className="navbar-brand"><NavLink
              className={`${classes["navbar-brand"]} ms-2 mt-3 flex `}
              to="/"
            >
              <img src={logo} className={classes.logo} alt="logo" />
            </NavLink></a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav flex ms-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <a className="nav-link " ><NavLink className={`${classes["nav-link"]}`} to="/admin-home">
                  Home
                </NavLink></a>
        </li>
        <li className="nav-item">
          <a className="nav-link " ><NavLink className={`${classes["nav-link"]}`} to="/admin-volunteers">
                  Volunteers
                </NavLink></a>
        </li>
        <li className="nav-item">
          <a className="nav-link " ><NavLink className={`${classes["nav-link"]}`} to="/admin-upload">
                  Upload Data
                </NavLink></a>
        </li>
        <li className="nav-item">
          <a className="nav-link " ><button className={`${classes.logoutBtn} btn btn-primary ms-4`} onClick={onLogoutHandler}>
                  Logout
                </button></a>
        </li>
        
      </ul>
    </div>
  </div>
</nav>
</>
  );
};

export default AdminNavbar;
