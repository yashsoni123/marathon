import React from "react";
import Container from "../Container/Container";
import Heading from "../Heading/Heading";
import classes from "./Footer.module.css";
import logo from "../../../Images/logo.png";

function Footer() {
  return (
    <footer className={classes.footer}>
      <div className="container-fluid mt-4">
        <div className="row ">
          <div className="col-sm-12 mx-auto">
            <div className="row flex justify-content-between">
              <div className="col-sm-3 col-xs-3 col-lg-3 col-md-3 center ">
                <img src={logo} alt="logo" className="img-fluid mt-3" />
              </div>

              <div className={`${classes.socialLinks} col-sm-3 center`}>
                <a
                  href="https://instagram.com/athletic_running_coach?utm_medium=copy_link"
                  target="_blank"
                >
                  <i className="fa fa-instagram"></i>
                </a>
                <a
                  href="https://www.facebook.com/profile.php?id=100058606403505"
                  target="_blank"
                >
                  <i className="fa fa-facebook-square"></i>
                </a>
              </div>
            </div>
            <div className="row mt-3 center">
              <div className={`${classes.copyRight} col-sm-10 center`}>
                Bhiwadi Half Marathon, For all enquiries please contact
                bhiwadihalfmarathon@gmail.com
              </div>
            </div>
            <div className="row center mt-3">
              <div className={`${classes.copyRight} col-sm-10 center`}>
                &nbsp;BhiwadiHalfMarathon&copy;2022. <br /> All Rights
                Reserved.Privacy Policy.
              </div>
            </div>
            <div className="row center mt-3">
              <div className={`${classes.copyRight} col-sm-10 center`}>
                Designed & developed by &nbsp;
                <span className="text-danger">HeyBytes.</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
