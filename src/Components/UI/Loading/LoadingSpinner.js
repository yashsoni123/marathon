import React from "react";
import classes from "./LoadingSpinner.module.css";

function LoadingSpinner() {
  return (
    <div className={classes.spinner}>
      <div class="d-flex justify-content-center">
        <div
          class="spinner-border"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span class="visually-hidden">Loading...</span>
        </div>
      </div>
    </div>
  );
}

export default LoadingSpinner;
