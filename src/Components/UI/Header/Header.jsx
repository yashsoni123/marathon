import React from "react";
import Container from "../Container/Container";
import classes from "./Header.module.css";

function Header(props) {
  return (
        <header className={classes.header}>
          <Container>

        <div className={classes.mainText}>
          <span className={classes.redText}> INDIA' S </span> PREMIERE
             <br /> <span className={classes.redText}>MARATHON </span> SPORTS
             <br /> <span className={classes.redText}>PLATFORM. </span>
        </div>
          </Container>
    </header>
  );
}

export default Header;
