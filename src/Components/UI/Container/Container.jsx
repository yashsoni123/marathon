import React from 'react';
import classes from "./Container.module.css";

const Container = props => {
    return (
        <div className="container-fluid text-center">
        <div className="row ">
          <div className="col-sm-11 col-lg-10 px-3  mx-auto">
              {props.children}
          </div>
        </div>
      </div>
    )
}

export default Container
