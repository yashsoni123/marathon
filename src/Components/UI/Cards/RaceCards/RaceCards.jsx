import React from "react";
import { useHistory } from "react-router-dom";
import card from "../../../../Images/card2.jpg";
import classes from "./RaceCards.module.css";

function RaceCards(props) {
  const history = useHistory();
  const onClickHandler = () => {
    history.push("/registration");
  };
  return (
    <div
      onClick={onClickHandler}
      className={`${classes.mainBox} card border-0 my-2 bg-transparent col-sm-8 col-xs-12 col-md-6 col-lg-4`}
    >
      <img src={card} className="card-img" alt="Card Image" />
      <div className={`${classes.overLay} card-img-overlay mb-auto `}>
        <h5 className={`${classes.head1} h2 card-title `}>{props.title}</h5>
        <div className={`${classes.head2} `}>
          <div>
            <i className="fa fa-calendar"></i> {props.date}
          </div>
          <div>
            {props.location}
            <i className="fa fa-map-marker"></i>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RaceCards;
