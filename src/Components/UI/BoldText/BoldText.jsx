import React from 'react';
import classes from "./BoldText.module.css";

function BoldText(props) {
    return (
        <span className={`${classes.boldText} ${classes.props}`}>{props.children}</span>
    )
}

export default BoldText
