import React from "react";

function Pagination({ totalPages, onClickHandler, activePage }) {
  const pages = [...Array(totalPages).keys()].map((num) => num + 1);
  return (
    <div>
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          {pages.map((num) => (
            <li className="page-item" key={num}>
              <a
                className={`${
                  num === activePage ? "bg-primary text-white" : ""
                } page-link`}
                onClick={onClickHandler.bind(null, num)}
              >
                {num}
              </a>
            </li>
          ))}
          {activePage < totalPages && (
            <li className="page-item">
              <a
                className="page-link"
                onClick={onClickHandler.bind(null, ++activePage)}
              >
                Next
              </a>
            </li>
          )}
        </ul>
      </nav>
    </div>
  );
}

export default Pagination;
