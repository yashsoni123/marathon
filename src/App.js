import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import Footer from "./Components/UI/Footer/Footer";
import React, { Suspense, lazy } from "react";

import Navbar from "./Components/UI/Navbar/Navbar";
import LoadingSpinner from "./Components/UI/Loading/LoadingSpinner";
import { useSelector } from "react-redux";
const Routes = lazy(() => import("./Routes/Routes"));

function App() {
  const authState = useSelector((state) => state);
  return (
    <>
      <Suspense fallback={<LoadingSpinner />}>
        {!authState.isUserLoggedIn && <Navbar />}
        <Routes />
        <Footer />
      </Suspense>
    </>
  );
}

export default App;
