import React, { useState, useEffect, Suspense, lazy } from "react";
import Registration from "../Components/Pages/Prices/Registration";
import GeneralInfo from "../Components/Pages/GeneralInfo/GeneralInfo";
import AboutUs from "../Components/Pages/About/AboutUs";
import PrizeMoney from "../Components/Pages/PrizeMoney/PrizeMoney";
import Media from "../Components/Pages/Media/Media";
import Races from "../Components/Pages/Races/Races";
import ContactUs from "../Components/Pages/ContactUs/ContactUs";
import Home from "../Components/Pages/Home/Home";
import VolunteerDetails from "../Components/Pages/Volunteer/VolunteerDetails";
import VolunteerRegistration from "../Components/Pages/Volunteer/VolunteerRegistration";
import RegistrationForm from "../Components/Pages/Prices/RegistrationForm";
import Login from "../Components/Pages/Login/Login";
import { Route, Switch, Redirect } from "react-router-dom";
import AdminHome from "../Components/Pages/Admin/AdminHome/AdminHome";
import AdminUpload from "../Components/Pages/Admin/AdminUpload/AdminUpload";
import AdminVolunteers from "../Components/Pages/Admin/AdminVolunteers/AdminVolunteers";
import ProtectedRoutes from "../Components/Pages/ProtectedRoutes";
import useAuth from "../Components/Custom Hooks/useAuth";
import { useCookies } from "react-cookie";
import { useHistory } from "react-router-dom";
import LoadingSpinner from "../Components/UI/Loading/LoadingSpinner";
// const AboutUs = lazy(() => import("../Components/Pages/About/AboutUs"));
const Routes = (props) => {
  const history = useHistory();
  const [cookies, setCookie] = useCookies();
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
  const [isAuth, login, logout] = useAuth(false);

  useEffect(() => {
    //   console.log(cookies.bhiwadiHalfMarathonAdmin);
    //   console.log(window.location.pathname);
    if (cookies.bhiwadiHalfMarathonAdmin) {
      setIsUserLoggedIn(true);
      if (window.location.pathname === "/") {
        history.push("/admin-home");
      }
    }
  }, [cookies]);

  return (
    <Switch>
      <Route path="/home" exact>
        <Home />
      </Route>
      <Route path="/about-us">
        {/* <Suspense fallback={<LoadingSpinner />}> */}
        <AboutUs />
        {/* </Suspense> */}
      </Route>
      <Route path="/registration" exact>
        <Registration />
      </Route>
      <Route path="/registration/:id" exact>
        <RegistrationForm />
      </Route>
      <Route path="/general-info">
        <GeneralInfo />
      </Route>
      <Route path="/media">
        <Media />
      </Route>
      <Route path="/races">
        <Races />
      </Route>
      <Route path="/volunteer/details" exact>
        <VolunteerDetails />
      </Route>
      <Route path="/volunteer/registration">
        <VolunteerRegistration />
      </Route>
      <Route path="/prize-money">
        <PrizeMoney />
      </Route>
      {/* <Route path="/admin-home">
          <AdminHome />
        </Route> */}
      <ProtectedRoutes
        path="/admin-home"
        component={AdminHome}
        isAuth={isUserLoggedIn}
      />
      <Route path="/admin-upload">
        <AdminUpload />
      </Route>

      {/* <Route path="/admin-volunteers">
          <AdminVolunteers />
        </Route> */}
      <ProtectedRoutes
        path="/admin-volunteers"
        component={AdminVolunteers}
        isAuth={isUserLoggedIn}
      />
      <Route path="/admin-login">
        <Login login={login} />
      </Route>
      <Route path="/contact-us">
        <ContactUs />
      </Route>
      <Route path="/" exact>
        <Redirect to="/home" exact />
      </Route>
    </Switch>
  );
};

export default React.memo(Routes);
